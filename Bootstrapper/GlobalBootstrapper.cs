﻿using ReactiveUI;
using System.Reflection;
using DI;

namespace Bootstrapper
{
	public class GlobalBootstrapper : RegistrationService
	{
		public void Register(IContainer container)
		{
			container.RegisterViewsForViewModels(Assembly.GetEntryAssembly());
			new Services.Bootstrapper().Register(container);
		}
	}
}