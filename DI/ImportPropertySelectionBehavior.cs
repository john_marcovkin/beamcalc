﻿using System;
using System.Linq;
using System.Reflection;
using SimpleInjector.Advanced;
using System.ComponentModel.Composition;

namespace DI
{
	class ImportPropertySelectionBehavior : IPropertySelectionBehavior
	{
		public bool SelectProperty(Type implementationType, PropertyInfo prop) =>
			prop.GetCustomAttributes(typeof(ImportAttribute)).Any();
	}
}
