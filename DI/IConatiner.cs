﻿using System;
using System.Reflection;
using System.Collections.Generic;

namespace DI
{
	/// <summary>DI container base methods.</summary>
	public interface IContainer
	{
		/// <summary>Resolves the specified service.</summary>
		TService GetService<TService>() where TService : class;

		/// <summary>Resolves the specified service.</summary>
		object GetService(Type serviceType);

		/// <summary>Gets all instances of the given serviceType currently registered in the container.</summary>
		/// <param name="serviceType">Type of object requested.</param>
		/// <returns>A sequence of instances of the requested serviceType.</returns>
		IEnumerable<object> GetServices(Type serviceType);

		/// <summary>Gets all instances of the given TService currently registered in the container.</summary>
		/// <typeparam name="TService">Type of object requested.</typeparam>
		/// <returns>A sequence of instances of the requested serviceType.</returns>
		IEnumerable<TService> GetServices<TService>() where TService : class;

		/// <summary>Registers that a new instance of TConcrete will be returned every time it is requested</summary>
		/// <typeparam name="TConcrete">The concrete type that will be registered.</typeparam>
		/// <param name="lifeStyle">The lifestyle that specifies how the returned instance will be cached.</param>
		void Register<TConcrete>(LifeStyle lifeStyle) where TConcrete : class;

		/// <summary>Registers that a new instance of TImplementation will be returned every time a TService is requested.</summary>
		/// <typeparam name="TService">The interface or base type that can be used to retrieve the instances.</typeparam>
		/// <typeparam name="TImplementation">The concrete type that will be registered.</typeparam>
		/// <param name="lifeStyle">The lifestyle that specifies how the returned instance will be cached.</param>
		void Register<TService, TImplementation>(LifeStyle lifeStyle)
			where TService : class
			where TImplementation : class, TService;

		/// <summary>Registers the specified delegate that allows returning instances of TService. 
		/// The delegate is expected to always return a new instance on each call.</summary>
		/// <typeparam name="TService">The interface or base type that can be used to retrieve instances.</typeparam>
		/// <param name="instanceCreator">The delegate that allows building or creating new instances.</param>
		/// <param name="lifeStyle">The lifestyle that specifies how the returned instance will be cached.</param>
		void Register<TService>(Func<TService> instanceCreator, LifeStyle lifeStyle) where TService : class;

		/// <summary>Registers that a new instance of concreteType will be returned every time it is requested.</summary>
		/// <param name="type">The concrete type that will be registered.</param>
		void Register(Type type);

		/// <summary>Registers all concrete, non-generic, public and internal types in the given 
		/// set of assemblies that implement the given openGenericServiceType with container's. 
		/// Decorators and generic type definitions will be excluded from registration, while 
		/// composites are included.</summary>
		/// <param name="type">The definition of the open generic type.</param>
		/// <param name="assemblies">A list of assemblies that will be searched.</param>
		/// <param name="lifeStyle">The lifestyle that specifies how the returned instance will be cached.</param>
		void Register(Type type, IEnumerable<Assembly> assemblies, LifeStyle lifeStyle);

		/// <summary>Registers all supplied implementationTypes based on the closed-generic version
		/// of the given openGenericServiceType with the transient lifestyle.</summary>
		/// <param name="type">The definition of the open generic type.</param>
		/// <param name="implementationTypes">A list types to be registered.</param>
		/// <param name="lifeStyle">The lifestyle that specifies how the returned instance will be cached.</param>
		void Register(Type type, IEnumerable<Type> implementationTypes, LifeStyle lifeStyle);

		/// <summary>Registers the specified delegate that allows returning instances of serviceType.</summary>
		/// <param name="type">The base type or interface to register.</param>
		/// <param name="instanceCreator">The delegate that will be used for creating new instances.</param>
		/// <param name="lifeStyle">The lifestyle that specifies how the returned instance will be cached.</param>
		void Register(Type type, Func<object> instanceCreator, LifeStyle lifeStyle);

		/// <summary>Registers that a new instance of implementationType will be returned every time a serviceType 
		/// is requested. If serviceType and implementationType represent the same type, the type is registered by 
		/// itself. Open and closed  generic types are supported.</summary>
		/// <param name="serviceType">he base type or interface to register. This can be an open-generic type.</param>
		/// <param name="implementationType">The actual type that will be returned when requested. This can be an open-generic type.</param>
		/// <param name="lifeStyle">The lifestyle that specifies how the returned instance will be cached.</param>
		void Register(Type serviceType, Type implementationType, LifeStyle lifeStyle);

		/// <summary>Registers an Action &lt;T&gt; delegate that runs after the creation of instances 
		/// that implement or derive from the given TService. Please note that only instances that are
		/// created by the container (using constructor injection) can be initialized this way.</summary>
		/// <typeparam name="TService">The type for which the initializer will be registered.</typeparam>
		/// <param name="instanceInitializer">The delegate that will be called after
		///  the instance has been constructed and before it is returned.</param>
		void RegisterInitializer<TService>(Action<TService> instanceInitializer) where TService : class;

		/// <summary>Registers a single concrete instance that will be constructed using constructor 
		/// injection and will be returned when this instance is requested by type TConcrete. This 
		/// TConcrete must be thread-safe when working in a multi-threaded environment. If TConcrete 
		/// implements IDisposable, a created instance will get disposed when Container.Dispose gets 
		/// called.</summary>
		/// <typeparam name="TConcrete">The concrete type that will be registered.</typeparam>
		void RegisterSingleton<TConcrete>() where TConcrete : class;

		/// <summary>Registers a single instance that will be returned when an instance 
		/// of type TService is requested. This instance must be thread-safe when working
		/// in a multi-threaded environment. 
		/// NOTE: Do note that instances supplied by this method NEVER get disposed by 
		/// the container, since the instance is assumed to outlive this container instance. 
		/// If disposing is required, use the overload that accepts a Func&lt;TResult> 
		/// delegate.</summary>
		/// <typeparam name="TService"></typeparam>
		/// <param name="instance"></param>
		void RegisterSingleton<TService>(TService instance) where TService : class;

		/// <summary>Registers the specified delegate that allows constructing a single instance 
		/// of TService. This delegate will be called at most once during the lifetime of the application.
		/// The returned instance must be thread-safe when working in a multi-threaded environment. 
		/// If the instance returned from instanceCreator implements IDisposable, the created instance 
		/// will get disposed when Container.Dispose gets called.</summary>
		/// <typeparam name="TService">The interface or base type that can be used to retrieve instances.</typeparam>
		/// <param name="instanceCreator">The delegate that allows building or creating this single instance.</param>
		void RegisterSingleton<TService>(Func<TService> instanceCreator) where TService : class;

		/// <summary>Registers that the same a single instance of type TImplementation will be returned every
		/// time an TService type is requested. If TService and TImplementation represent the same type, the type
		/// is registered by itself. TImplementation must be thread-safe when working in a multi-threaded environment.
		/// If TImplementation implements System.IDisposable, a created instance will get disposed when SimpleInjector.
		/// Container.Dispose gets called.</summary>
		void RegisterSingleton<TService, TImplementation>()
			where TService : class
			where TImplementation : class, TService;
		
		void RegisterCollection<TService>(IEnumerable<Assembly> assemblies) where TService : class;

		void RegisterCollection<TService>(IEnumerable<Type> serviceTypes) where TService : class;

		void RegisterCollection<TService>(IEnumerable<TService> containerUncontrolledCollection) where TService : class;

		void RegisterCollection<TService>(params TService[] singletons) where TService : class;

		void Verify();
	}
}
