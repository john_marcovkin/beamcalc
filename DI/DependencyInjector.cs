﻿using Splat;
using System;
using SimpleInjector;
using System.Reflection;
using System.Collections.Generic;

namespace DI
{
	public class DependencyInjector : IContainer, IMutableDependencyResolver
	{
		public DependencyInjector()
		{
			_Container = new Container();
			_Container.Options.PropertySelectionBehavior = new ImportPropertySelectionBehavior();

			_Resolver = new ModernDependencyResolver();
		}

		/// <summary>SimpleInjector container.</summary>
		private readonly Container _Container;

		/// <summary>Splat service locator.</summary>
		private readonly IMutableDependencyResolver _Resolver;


		#region SIMPLE INJECTOR

		public void Register<TConcrete>(LifeStyle lifeStyle) where TConcrete : class
		{
			switch (lifeStyle)
			{
				case LifeStyle.Transient: _Container.Register<TConcrete>(); break;
				case LifeStyle.Singleton: _Container.Register<TConcrete>(Lifestyle.Singleton); break;
				case LifeStyle.Scoped: _Container.Register<TConcrete>(Lifestyle.Scoped); break;
				default: _Container.Register<TConcrete>(); break;
			}
		}

		public void Register<TService, TImplementation>(LifeStyle lifeStyle)
			where TService : class
			where TImplementation : class, TService
		{
			switch (lifeStyle)
			{
				case LifeStyle.Transient: _Container.Register<TService, TImplementation>(); break;
				case LifeStyle.Singleton: _Container.Register<TService, TImplementation>(Lifestyle.Singleton); break;
				case LifeStyle.Scoped: _Container.Register<TService, TImplementation>(Lifestyle.Scoped); break;
				default: _Container.Register<TService, TImplementation>(); break;
			}
		}

		public void Register<TService>(Func<TService> instanceCreator, LifeStyle lifeStyle)
			where TService : class
		{
			switch (lifeStyle)
			{
				case LifeStyle.Transient: _Container.Register(instanceCreator); break;
				case LifeStyle.Singleton: _Container.Register(instanceCreator, Lifestyle.Singleton); break;
				case LifeStyle.Scoped: _Container.Register(instanceCreator, Lifestyle.Scoped); break;
				default: _Container.Register(instanceCreator, Lifestyle.Transient); break;
			}
		}

		public void Register(Type type) => _Container.Register(type);

		public void Register(Type type, IEnumerable<Assembly> assemblies, LifeStyle lifeStyle)
		{
			switch (lifeStyle)
			{
				case LifeStyle.Transient: _Container.Register(type, assemblies); break;
				case LifeStyle.Singleton: _Container.Register(type, assemblies, Lifestyle.Singleton); break;
				case LifeStyle.Scoped: _Container.Register(type, assemblies, Lifestyle.Scoped); break;
				default: _Container.Register(type, assemblies); break;
			}
		}

		public void Register(Type type, IEnumerable<Type> implementationTypes, LifeStyle lifeStyle)
		{
			switch (lifeStyle)
			{
				case LifeStyle.Transient: _Container.Register(type, implementationTypes); break;
				case LifeStyle.Singleton: _Container.Register(type, implementationTypes, Lifestyle.Singleton); break;
				case LifeStyle.Scoped: _Container.Register(type, implementationTypes, Lifestyle.Scoped); break;
				default: _Container.Register(type, implementationTypes); break;
			}
		}

		public void Register(Type type, Func<object> instanceCreator, LifeStyle lifeStyle)
		{
			switch (lifeStyle)
			{
				case LifeStyle.Transient: _Container.Register(type, instanceCreator); break;
				case LifeStyle.Singleton: _Container.Register(type, instanceCreator, Lifestyle.Singleton); break;
				case LifeStyle.Scoped: _Container.Register(type, instanceCreator, Lifestyle.Scoped); break;
				default: _Container.Register(type, instanceCreator); break;
			}
		}

		public void Register(Type serviceType, Type implementationType, LifeStyle lifeStyle)
		{
			switch (lifeStyle)
			{
				case LifeStyle.Transient: _Container.Register(serviceType, implementationType); break;
				case LifeStyle.Singleton: _Container.Register(serviceType, implementationType, Lifestyle.Singleton); break;
				case LifeStyle.Scoped: _Container.Register(serviceType, implementationType, Lifestyle.Scoped); break;
				default: _Container.Register(serviceType, implementationType); break;
			}
		}


		public void RegisterInitializer<TService>(Action<TService> instanceInitializer) where TService : class => 
			_Container.RegisterInitializer(instanceInitializer);


		public void RegisterSingleton<TConcrete>() where TConcrete : class => _Container.RegisterSingleton<TConcrete>();

		public void RegisterSingleton<TService>(TService instance) where TService : class => _Container.RegisterInstance(instance);

		public void RegisterSingleton<TService>(Func<TService> instanceCreator) where TService : class => 
			_Container.RegisterSingleton(instanceCreator);

		public void RegisterSingleton<TService, TImplementation>()
			where TService : class
			where TImplementation : class, TService =>
			_Container.RegisterSingleton<TService, TImplementation>();

		public void RegisterCollection<TService>(IEnumerable<Assembly> assemblies) where TService : class => 
			_Container.Collection.Register<TService>(assemblies);

		public void RegisterCollection<TService>(IEnumerable<Type> serviceTypes) where TService : class => 
			_Container.Collection.Register<TService>(serviceTypes);

		public void RegisterCollection<TService>(IEnumerable<TService> containerUncontrolledCollection)
			where TService : class => _Container.Collection.Register(containerUncontrolledCollection);

		public void RegisterCollection<TService>(params TService[] singletons) where TService : class => 
			_Container.Collection.Register(singletons);
		
		public void Verify() => _Container.Verify();

		#endregion SIMPLE INJECTOR


		#region BASE GET SERVICE

		public object GetService(Type serviceType, string contract)
		{
			var service = _Resolver.GetService(serviceType, contract);
			if (service != null) return service;

			try { return _Container.GetInstance(serviceType); }
			catch { return null; }
		}

		public IEnumerable<object> GetServices(Type serviceType, string contract)
		{
			var services = _Resolver.GetServices(serviceType, contract);
			if (services != null) return services;

			try { return _Container.GetAllInstances(serviceType); }
			catch { return new object[0]; }
		}
			 

		#endregion BASE GET SERVICE


		#region EXPLICIT INTERFACES METHODS

		object IContainer.GetService(Type serviceType) => GetService(serviceType, null);
		TService IContainer.GetService<TService>() => (TService) GetService(typeof(TService), null);
		IEnumerable<object> IContainer.GetServices(Type serviceType) => GetServices(serviceType, null);
		IEnumerable<TService> IContainer.GetServices<TService>() => (IEnumerable<TService>) GetServices(typeof(TService), null);

		
		object IDependencyResolver.GetService(Type serviceType, string contract) => GetService(serviceType, contract);
		IEnumerable<object> IDependencyResolver.GetServices(Type serviceType, string contract) => GetServices(serviceType, contract);

		#endregion EXPLICIT INTERFACES METHODS


		public void Register(Func<object> factory, Type serviceType, string contract) =>
			_Resolver.Register(factory, serviceType, contract);
		

		public void UnregisterCurrent(Type serviceType, string contract) =>
			_Resolver.UnregisterCurrent(serviceType, contract);

		public void UnregisterAll(Type serviceType, string contract) =>
			_Resolver.UnregisterAll(serviceType, contract);


		public void Dispose()
		{
			_Container.Dispose();
			_Resolver.Dispose();
		}

		public IDisposable ServiceRegistrationCallback(Type serviceType, string contract, Action<IDisposable> callback) =>
			_Resolver.ServiceRegistrationCallback(serviceType, contract, callback);
	}
}