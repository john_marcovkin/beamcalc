﻿namespace DI
{
	public interface IRegistrationService
	{
		IContainer RegisterDependencies(IContainer container);
	}
}