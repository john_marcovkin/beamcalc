﻿namespace DI
{
	/// <summary>The life-time of an object.</summary>
	public enum LifeStyle
	{
		Scoped,
		Singleton,
		Transient
	}
}