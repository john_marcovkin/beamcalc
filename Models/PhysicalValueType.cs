﻿namespace Models
{
	public enum PhysicalValueType
	{
		None,

		Width,
		Width0,
		Width1,
		Width2,

		Height,
		Height0,
		Height1,
		Height2,

		Length,

		Load,
		SoleWeight,

		Crack,
		Friction,
		Deflection,

		ConcreteTension,
		ConcretePoissonRate,
		ConcreteCompression,
		ConcretePrismStrength,
		ConcreteTensionStrength,
		ConcreteElasticityModulus,

		ReinforcementTension,
		ReinforcementPrestress,
		ReinforcementSectionArea,
		ReinforcementLocationHeight,
		ReinforcementTensionStrength,
		ReinforcementElasticityModulus
	}
}