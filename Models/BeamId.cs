﻿using System;
using System.Globalization;
using System.ComponentModel;

namespace Models
{
	[TypeConverter(typeof(BeamDiagramIdTypeConverter))]
    public class BeamId
    {
	    public BeamId(BeamType beamType, TestPhase testPhase, int testOrdinal)
	    {
		    BeamType = beamType;
		    TestPhase = testPhase;
		    TestDiagramNumber = testOrdinal;
	    }

		public BeamType BeamType { get; }
		public TestPhase TestPhase { get; }
		public int TestDiagramNumber { get; }

	    public override string ToString() => 
			$"{BeamType.ToString()};{TestPhase.ToString()};{TestDiagramNumber}";
    }

	public class BeamDiagramIdTypeConverter: TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType) =>
			sourceType == typeof(string);

		/// <summary>Converts a string similar to "Beam01; design; 0" to a BeamId object.</summary>
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			var testNumber = 0;
			var type = BeamType.None;
			var testPhase = TestPhase.None;

			var parameters = ((string) value).Split(';');
			foreach (var parameter in parameters)
			{
				var p = parameter.Trim();

				if (Enum.TryParse(p, true, out type) || 
					Enum.TryParse(p, true, out testPhase))
					continue;

				int.TryParse(p, out testNumber);
			}

			return new BeamId(type, testPhase, testNumber);
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) =>
			destinationType == typeof(string);

		/// <summary>Converts a BeamId object to a string similar to "Beam01;design;0".</summary>
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value,
			Type destinationType) => ((BeamId) value).ToString();
	}
}