﻿namespace Models
{
	public enum BeamType
	{
		None,
		FixedBeam,
		ReinforcedBeam,
		PrestressedBeam,
		DeepBeam
	}
}