﻿namespace Models
{
	public class BeamInputParameters
	{
		/// <summary>The length of the beam.</summary>
		public double Length { get; set; }

		public double Width { get; set; }

		/// <summary>The width of the top of beam.</summary>
		public double Width0 { get; set; }

		/// <summary>The width of the middle of beam.</summary>
		public double Width1 { get; set; }

		/// <summary>The width of the bottom of beam.</summary>
		public double Width2 { get; set; }


		public double Height { get; set; }

		/// <summary>The height of the top of beam.</summary>
		public double Height0 { get; set; }

		/// <summary>The height of middle top of beam.</summary>
		public double Height1 { get; set; }

		/// <summary>The height of the bottom of beam.</summary>
		public double Height2 { get; set; }

		public double Friction { get; set; }

		public double ConcretePoissonRate { get; set; } = 0.2D;
		public double ConcretePrismStrength { get; set; }
		public double ConcreteTensionStrength { get; set; }
		public double ConcreteElasticityModulus { get; set; }

		public double ReinforcementPrestress { get; set; }
		public double ReinforcementSectionArea { get; set; }
		public double ReinforcementLocationHeight { get; set; }
		public double ReinforcementTensionStrength { get; set; }
		public double ReinforcementElasticityModulus { get; set; }

		public double BeamWeight { get; set; }

		/// <summary>This limits calculator if it cannot reach the failure phase of the beam.</summary>
		public int MaxIterationsNumber { get; set; }

		/// <summary>Load step chosen on the user interface.</summary>
		public double OutputLoadStep { get; set; }

		/// <summary>Load step used by calculator. Set it to get more precise results.</summary>
		public double CalculationLoadStep { get; set; }

		public BeamType BeamType { get; set; }
		public MeasureUnitsSystem UnitsSystem { get; set; }

		public BeamInputParameters Clone() => new BeamInputParameters
		{
			Length = this.Length,
			Width = this.Width,
			Width0 = this.Width0,
			Width1 = this.Width1,
			Width2 = this.Width2,
			Height = this.Height,
			Height0 = this.Height0,
			Height1 = this.Height1,
			Height2 = this.Height2,

			Friction = this.Friction,
			ConcretePoissonRate = this.ConcretePoissonRate,
			ConcretePrismStrength = this.ConcretePrismStrength,
			ConcreteTensionStrength = this.ConcreteTensionStrength,
			ConcreteElasticityModulus = this.ConcreteElasticityModulus,

			ReinforcementPrestress = this.ReinforcementPrestress,
			ReinforcementSectionArea = this.ReinforcementSectionArea,
			ReinforcementLocationHeight = this.ReinforcementLocationHeight,
			ReinforcementTensionStrength = this.ReinforcementTensionStrength,
			ReinforcementElasticityModulus = this.ReinforcementElasticityModulus,

			BeamType = this.BeamType,
			BeamWeight = this.BeamWeight,
			UnitsSystem = this.UnitsSystem,
			OutputLoadStep = this.OutputLoadStep,
			CalculationLoadStep = this.CalculationLoadStep,
			MaxIterationsNumber = this.MaxIterationsNumber
		};
	}
}