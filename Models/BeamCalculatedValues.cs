﻿namespace Models
{
	public class BeamCalculatedValues
	{
		public double Load { get; set; }
		public double BeamWeight { get; set; }
		public double ConcreteTension { get; set; }

		public double ConcreteCompression { get; set; }
		public double ReinforcementTension { get; set; }

		public double Deflection { get; set; }
		public double CracksWidth { get; set; }
		public double CracksHeight { get; set; }
	}
}