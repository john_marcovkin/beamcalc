﻿namespace Models
{
    public enum TestPhase
    {
		None,
		Design,
		NoLoad,
		DeflectedUp,
		DeflectedDown,
	    DeflectedDownBeforeCracks,
	    DeflectedDownCracksDevelopment,
		Failure
    }
}