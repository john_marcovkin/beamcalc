﻿using ReactiveUI;
using BeamCalculator.ViewModels;
using System.Reactive.Disposables;
using System.Windows.Controls;

namespace BeamCalculator.Views
{
	public class BeamParametersBase : ReactiveUserControl<BeamParametersViewModel> { } 

	public partial class BeamParameters
    {
        public BeamParameters()
        {
            InitializeComponent();
	        this.WhenActivated(Initialize);
		}

	    public void Initialize(CompositeDisposable d)
	    {
		    //this.ReinforcementSectionArea.EquationSmallNumberBound = 0.001;
		    //this.ReinforcementSectionArea.EquationSmallNumberFormatString = "E6";

			// Beam sizes:
		    this.Bind(ViewModel, vm => vm.Length, v => v.Length.EquationValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MinLength, v => v.Length.EquationMinValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MaxLength, v => v.Length.EquationMaxValue).DisposeWith(d);


			// Height:
		    this.Bind(ViewModel, vm => vm.Height, v => v.Height.EquationValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MinHeight, v => v.Height.EquationMinValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MaxHeight, v => v.Height.EquationMaxValue).DisposeWith(d);

		    this.Bind(ViewModel, vm => vm.Height0, v => v.Height0.EquationValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MinHeight0, v => v.Height0.EquationMinValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MaxHeight0, v => v.Height0.EquationMaxValue).DisposeWith(d);

		    this.Bind(ViewModel, vm => vm.Height1, v => v.Height1.EquationValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MinHeight1, v => v.Height1.EquationMinValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MaxHeight1, v => v.Height1.EquationMaxValue).DisposeWith(d);

		    this.Bind(ViewModel, vm => vm.Height2, v => v.Height2.EquationValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MinHeight2, v => v.Height2.EquationMinValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MaxHeight2, v => v.Height2.EquationMaxValue).DisposeWith(d);


			// Width:
		    this.Bind(ViewModel, vm => vm.Width, v => v.Width.EquationValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MinWidth, v => v.Width.EquationMinValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MaxWidth, v => v.Width.EquationMaxValue).DisposeWith(d);

		    this.Bind(ViewModel, vm => vm.Width0, v => v.Width0.EquationValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MinWidth0, v => v.Width0.EquationMinValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MaxWidth0, v => v.Width0.EquationMaxValue).DisposeWith(d);

		    this.Bind(ViewModel, vm => vm.Width1, v => v.Width1.EquationValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MinWidth1, v => v.Width1.EquationMinValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MaxWidth1, v => v.Width1.EquationMaxValue).DisposeWith(d);

		    this.Bind(ViewModel, vm => vm.Width2, v => v.Width2.EquationValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MinWidth2, v => v.Width2.EquationMinValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MaxWidth2, v => v.Width2.EquationMaxValue).DisposeWith(d);



			// Beam weight:
		    this.Bind(ViewModel, vm => vm.SoleWeight, v => v.SoleWeight.EquationValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MinSoleWeight, v => v.SoleWeight.EquationMinValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MaxSoleWeight, v => v.SoleWeight.EquationMaxValue).DisposeWith(d);

			// Friction:
		    this.Bind(ViewModel, vm => vm.Friction, v => v.Friction.EquationValue, Friction.Events().LostFocus).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MinFriction, v => v.Friction.EquationMinValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MaxFriction, v => v.Friction.EquationMaxValue).DisposeWith(d);

			// Concrete parameters:
			this.Bind(ViewModel, vm => vm.ConcretePoissonRate, v => v.ConcretePoissonRate.EquationValue).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.MinConcretePoissonRate, v => v.ConcretePoissonRate.EquationMinValue).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.MaxConcretePoissonRate, v => v.ConcretePoissonRate.EquationMaxValue).DisposeWith(d);

			this.Bind(ViewModel, vm => vm.ConcreteElasticityModulus, v => v.ConcreteElasticityModulus.EquationValue).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.MinConcreteElasticityModulus, v => v.ConcreteElasticityModulus.EquationMinValue).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.MaxConcreteElasticityModulus, v => v.ConcreteElasticityModulus.EquationMaxValue).DisposeWith(d);

			this.Bind(ViewModel, vm => vm.ConcretePrismStrength, v => v.ConcretePrismStrength.EquationValue).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.MinConcretePrismStrength, v => v.ConcretePrismStrength.EquationMinValue).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.MaxConcretePrismStrength, v => v.ConcretePrismStrength.EquationMaxValue).DisposeWith(d);

			this.Bind(ViewModel, vm => vm.ConcreteTensionStrength, v => v.ConcreteTensionStrength.EquationValue).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.MinConcreteTensionStrength, v => v.ConcreteTensionStrength.EquationMinValue).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.MaxConcreteTensionStrength, v => v.ConcreteTensionStrength.EquationMaxValue).DisposeWith(d);

			// Reinforcement parameters:
			this.Bind(ViewModel, vm => vm.ReinforcementSectionArea, v => v.ReinforcementSectionArea.EquationValue).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.MinReinforcementSectionArea, v => v.ReinforcementSectionArea.EquationMinValue).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.MaxReinforcementSectionArea, v => v.ReinforcementSectionArea.EquationMaxValue).DisposeWith(d);

		    this.Bind(ViewModel, vm => vm.ReinforcementElasticityModulus, v => v.ReinforcementElasticityModule.EquationValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MinReinforcementElasticityModulus, v => v.ReinforcementElasticityModule.EquationMinValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MaxReinforcementElasticityModulus, v => v.ReinforcementElasticityModule.EquationMaxValue).DisposeWith(d);

		    this.Bind(ViewModel, vm => vm.ReinforcementTensionStrength, v => v.ReinforcementTensionStrength.EquationValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MinReinforcementTensionStrength, v => v.ReinforcementTensionStrength.EquationMinValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MaxReinforcementTensionStrength, v => v.ReinforcementTensionStrength.EquationMaxValue).DisposeWith(d);

		    this.Bind(ViewModel, vm => vm.ReinforcementPrestress, v => v.ReinforcementPrestress.EquationValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MinReinforcementPrestress, v => v.ReinforcementPrestress.EquationMinValue).DisposeWith(d);
		    this.Bind(ViewModel, vm => vm.MaxReinforcementPrestress, v => v.ReinforcementPrestress.EquationMaxValue).DisposeWith(d);

			this.Bind(ViewModel, vm => vm.ReinforcementLocationHeight, v => v.ReinforcementLocationHeight.EquationValue).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.MinReinforcementLocationHeight, v => v.ReinforcementLocationHeight.EquationMinValue).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.MaxReinforcementLocationHeight, v => v.ReinforcementLocationHeight.EquationMaxValue).DisposeWith(d);


			// Show/Hide standard size parameters
		    this.OneWayBind(ViewModel, vm => vm.ShowStandardSizeParameters, v => v.Width.Visibility).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.ShowStandardSizeParameters, v => v.Height.Visibility).DisposeWith(d);

		    // Show/Hide extended size parameters
			this.OneWayBind(ViewModel, vm => vm.ShowExtendedSizeParameters, v => v.Width0.Visibility).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.ShowExtendedSizeParameters, v => v.Width1.Visibility).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.ShowExtendedSizeParameters, v => v.Width2.Visibility).DisposeWith(d);

			this.OneWayBind(ViewModel, vm => vm.ShowExtendedSizeParameters, v => v.Height0.Visibility).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.ShowExtendedSizeParameters, v => v.Height1.Visibility).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.ShowExtendedSizeParameters, v => v.Height2.Visibility).DisposeWith(d);

			// Show/Hide friction parameter:
		    this.OneWayBind(ViewModel, vm => vm.ShowFrictionParameter, v => v.Friction.Visibility).DisposeWith(d);

			// Show/Hide reinforcement parameters:
			this.OneWayBind(ViewModel, vm => vm.ShowReinforcementPrestress, v => v.ReinforcementPrestress.Visibility).DisposeWith(d);
			this.OneWayBind(ViewModel, vm => vm.ShowBaseReinforcementParameters, v => v.ReinforcementSectionArea.Visibility).DisposeWith(d);
			this.OneWayBind(ViewModel, vm => vm.ShowBaseReinforcementParameters, v => v.ReinforcementLocationHeight.Visibility).DisposeWith(d);
			this.OneWayBind(ViewModel, vm => vm.ShowBaseReinforcementParameters, v => v.ReinforcementTensionStrength.Visibility).DisposeWith(d);
			this.OneWayBind(ViewModel, vm => vm.ShowBaseReinforcementParameters, v => v.ReinforcementElasticityModule.Visibility).DisposeWith(d);


			// Units system:
		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.Length.EquationMeasureUnitsSystem).DisposeWith(d);

		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.Width.EquationMeasureUnitsSystem).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.Width0.EquationMeasureUnitsSystem).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.Width1.EquationMeasureUnitsSystem).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.Width2.EquationMeasureUnitsSystem).DisposeWith(d);

		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.Height.EquationMeasureUnitsSystem).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.Height0.EquationMeasureUnitsSystem).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.Height1.EquationMeasureUnitsSystem).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.Height2.EquationMeasureUnitsSystem).DisposeWith(d);

		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.SoleWeight.EquationMeasureUnitsSystem).DisposeWith(d);

		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.ConcretePrismStrength.EquationMeasureUnitsSystem).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.ConcreteTensionStrength.EquationMeasureUnitsSystem).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.ConcreteElasticityModulus.EquationMeasureUnitsSystem).DisposeWith(d);

		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.ReinforcementPrestress.EquationMeasureUnitsSystem).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.ReinforcementSectionArea.EquationMeasureUnitsSystem).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.ReinforcementLocationHeight.EquationMeasureUnitsSystem).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.ReinforcementTensionStrength.EquationMeasureUnitsSystem).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.UnitsSystem, v => v.ReinforcementElasticityModule.EquationMeasureUnitsSystem).DisposeWith(d);

			// Number formatting:
			/*
		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.Length.EquationBaseNumberFormatString).DisposeWith(d);

		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.Width.EquationBaseNumberFormatString).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.Width0.EquationBaseNumberFormatString).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.Width1.EquationBaseNumberFormatString).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.Width2.EquationBaseNumberFormatString).DisposeWith(d);

		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.Height.EquationBaseNumberFormatString).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.Height0.EquationBaseNumberFormatString).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.Height1.EquationBaseNumberFormatString).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.Height2.EquationBaseNumberFormatString).DisposeWith(d);
			
		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.SoleWeight.EquationBaseNumberFormatString).DisposeWith(d);

		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.ConcretePrismStrength.EquationBaseNumberFormatString).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.ConcreteTensionStrength.EquationBaseNumberFormatString).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.ConcreteElasticityModulus.EquationBaseNumberFormatString).DisposeWith(d);

		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.ReinforcementPrestress.EquationBaseNumberFormatString).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.ReinforcementLocationHeight.EquationBaseNumberFormatString).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.ReinforcementTensionStrength.EquationBaseNumberFormatString).DisposeWith(d);
		    this.OneWayBind(ViewModel, vm => vm.BaseNumberFormatString, v => v.ReinforcementElasticityModule.EquationBaseNumberFormatString).DisposeWith(d);
			*/
		}
	}
}