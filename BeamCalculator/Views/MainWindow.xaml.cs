﻿using Splat;
using System;
using Models;
using ReactiveUI;
using System.Linq;
using System.Reactive.Linq;
using BeamCalculator.ViewModels;
using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Disposables;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace BeamCalculator.Views
{
	public class MainWidowBase : ReactiveWindow<MainWindowViewModel> { }

	public partial class MainWindow
	{

		public MainWindow()
		{
			InitializeComponent();

			ViewModel = (MainWindowViewModel)Locator.CurrentMutable.GetService(typeof(MainWindowViewModel));
			ViewModel.Initialize();

			this.InitializeMessages();
			this.WhenActivated(InitializeReactiveUI);
		}

		private string _ErrorCaption;
		private string _CalculationError;

		private void InitializeMessages()
		{
			_ErrorCaption = (string) FindResource("MessageBox.Error.Caption");
			_CalculationError = (string) FindResource("MessageBox.Error.CalculationFailed");
		}
		
		private void InitializeReactiveUI(CompositeDisposable d)
		{
			// Menu:
			this.OneWayBind(ViewModel, vm => vm.BeamMenuItems, v => v.MenuListBox.ItemsSource).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.SelectedMenuItem, v => v.MenuListBox.SelectedItem).DisposeWith(d);
			
			this.MenuListBox.Events().SelectionChanged
				.Select(x => ((IEnumerable<object>)x.AddedItems).FirstOrDefault())
				.OfType<BeamMenuItemModel>()
				.InvokeCommand(this, x => x.ViewModel.OnMenuItemChangedCommand)
				.DisposeWith(d);

			// Beam Viewer:
			this.OneWayBind(ViewModel, vm => vm.BeamViewerContext, v => v.BeamViewer.Content).DisposeWith(d);
			
			// Navigator:
			this.Bind(ViewModel, vm => vm.NavigatorContext, v => v.Navigator.ViewModel).DisposeWith(d);

			// Parameters:
			this.Bind(ViewModel, vm => vm.BeamParametersContext, v => v.BeamParameters.ViewModel).DisposeWith(d);


			// Measure units system:
			this.WhenAnyValue(v => v.SiRadioButton.IsChecked)
				.Subscribe(isChecked =>
				{
					ViewModel.UnitsSystem = (isChecked == true)
						? MeasureUnitsSystem.Si 
						: MeasureUnitsSystem.Tech;
				})
				.DisposeWith(d);

			this.WhenAnyValue(v => v.ViewModel.UnitsSystem)
				.Where(us => us != MeasureUnitsSystem.None)
				.Subscribe(us =>
				{
					SiRadioButton.IsChecked = (us == MeasureUnitsSystem.Si);
					TechRadioButton.IsChecked = (us == MeasureUnitsSystem.Tech);
				})
				.DisposeWith(d);

			// Load-steps combobox:
			this.LoadStep.DisplayMemberPath = nameof(NumberItemModelBase.DisplayText);
			this.OneWayBind(ViewModel, vm => vm.LoadSteps, v => v.LoadStep.ItemsSource).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.LoadStepValue, v => v.LoadStep.SelectedItem).DisposeWith(d);

			// Commands:
			this.BindCommand(ViewModel, vm => vm.CalculateCommand, v => v.CalculateButton).DisposeWith(d);
			this.BindCommand(ViewModel, vm => vm.ClearCommand, v => v.ClearButton).DisposeWith(d);

			// Messages:
			this.ViewModel.NotifyCalculationError.RegisterHandler(i => OnNotifyCalculationError(i));
		}

		private void OnNotifyCalculationError(InteractionContext<Unit, Unit> interaction)
		{
			MessageBox.Show(_CalculationError, _ErrorCaption, MessageBoxButton.OK, MessageBoxImage.Error);
			interaction.SetOutput(Unit.Default);
		}
	}
}