﻿using System.Windows.Controls;

namespace BeamCalculator.Views.Beams.PrestressedBeamWithFriction
{
	/// <summary>
	/// Interaction logic for DeflectedUp01.xaml
	/// </summary>
	public partial class DeflectedUp01 : UserControl
	{
		public DeflectedUp01()
		{
			InitializeComponent();
		}
	}
}
