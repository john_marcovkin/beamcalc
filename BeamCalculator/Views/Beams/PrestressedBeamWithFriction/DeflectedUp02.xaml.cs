﻿using System.Windows.Controls;

namespace BeamCalculator.Views.Beams.PrestressedBeamWithFriction
{
    /// <summary>
    /// Interaction logic for DeflectedUp02.xaml
    /// </summary>
    public partial class DeflectedUp02 : UserControl
    {
        public DeflectedUp02()
        {
            InitializeComponent();
        }
    }
}
