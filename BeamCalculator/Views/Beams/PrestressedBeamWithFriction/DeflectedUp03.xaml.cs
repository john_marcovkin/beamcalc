﻿using System.Windows.Controls;

namespace BeamCalculator.Views.Beams.PrestressedBeamWithFriction
{
    /// <summary>
    /// Interaction logic for DeflectedUp03.xaml
    /// </summary>
    public partial class DeflectedUp03 : UserControl
    {
        public DeflectedUp03()
        {
            InitializeComponent();
        }
    }
}
