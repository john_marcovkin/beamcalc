﻿using System.Windows.Controls;

namespace BeamCalculator.Views.Beams.PrestressedBeamWithFriction
{
	/// <summary>
	/// Interaction logic for DeflectedDown01.xaml
	/// </summary>
	public partial class DeflectedDown01 : UserControl
	{
		public DeflectedDown01()
		{
			InitializeComponent();
		}
	}
}
