﻿using System.Windows.Controls;

namespace BeamCalculator.Views.Beams.PrestressedBeamWithFriction
{
    /// <summary>
    /// Interaction logic for DeflectedDown04.xaml
    /// </summary>
    public partial class DeflectedDown04 : UserControl
    {
        public DeflectedDown04()
        {
            InitializeComponent();
        }
    }
}
