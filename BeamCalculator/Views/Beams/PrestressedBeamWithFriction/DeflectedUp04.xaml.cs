﻿using System.Windows.Controls;

namespace BeamCalculator.Views.Beams.PrestressedBeamWithFriction
{
    /// <summary>
    /// Interaction logic for DeflectedUp03.xaml
    /// </summary>
    public partial class DeflectedUp04 : UserControl
    {
        public DeflectedUp04()
        {
            InitializeComponent();
        }
    }
}
