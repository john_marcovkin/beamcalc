﻿using System.Windows.Controls;

namespace BeamCalculator.Views.Beams.ReinforcedBeamWithFriction
{
    /// <summary>
    /// Interaction logic for DeflectedDown03.xaml
    /// </summary>
    public partial class DeflectedDown03 : UserControl
    {
        public DeflectedDown03()
        {
            InitializeComponent();
        }
    }
}
