﻿using System.Windows.Controls;

namespace BeamCalculator.Views.Beams.ReinforcedBeamWithFriction
{
    /// <summary>
    /// Interaction logic for Failure.xaml
    /// </summary>
    public partial class Failure : UserControl
    {
        public Failure()
        {
            InitializeComponent();
        }
    }
}
