﻿using System.Windows.Controls;

namespace BeamCalculator.Views.Beams.ReinforcedBeamWithFriction
{
    /// <summary>
    /// Interaction logic for DeflectedDown02.xaml
    /// </summary>
    public partial class DeflectedDown02 : UserControl
    {
        public DeflectedDown02()
        {
            InitializeComponent();
        }
    }
}
