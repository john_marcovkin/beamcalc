﻿using System.Windows.Controls;

namespace BeamCalculator.Views.Beams.FixedBeam
{
    /// <summary>
    /// Interaction logic for B04.xaml
    /// </summary>
    public partial class DeflectedDown03 : UserControl
    {
        public DeflectedDown03()
        {
            InitializeComponent();
        }
    }
}
