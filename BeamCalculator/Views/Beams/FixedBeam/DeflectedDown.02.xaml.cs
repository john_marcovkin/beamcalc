﻿using System.Windows.Controls;

namespace BeamCalculator.Views.Beams.FixedBeam
{
    /// <summary>
    /// Interaction logic for B03.xaml
    /// </summary>
    public partial class DeflectedDown02 : UserControl
    {
        public DeflectedDown02()
        {
            InitializeComponent();
        }
    }
}
