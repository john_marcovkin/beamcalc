﻿using System.Windows.Controls;

namespace BeamCalculator.Views.Beams.FixedBeam
{
    /// <summary>
    /// Interaction logic for B01.xaml
    /// </summary>
    public partial class DeflectedDown00 : UserControl
    {
        public DeflectedDown00()
        {
            InitializeComponent();
        }
    }
}