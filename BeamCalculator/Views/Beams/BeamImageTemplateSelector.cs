﻿using Models;
using System;
using System.Windows;
using System.Windows.Controls;
using BeamCalculator.ViewModels;

namespace BeamCalculator.Views.Beams
{
	public class BeamImageTemplateSelector : DataTemplateSelector
	{
		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			if (!(item is BeamCalcDataViewModel viewModel) || viewModel.BeamId == null) return null;

			switch (viewModel.BeamId.BeamType)
			{
				case BeamType.FixedBeam: return SelectFixedBeamTemplate(viewModel);
				case BeamType.ReinforcedBeam: return SelectReinforcedBeamTemplate(viewModel);
				case BeamType.PrestressedBeam: return SelectPrestressedBeamTemplate(viewModel);
				default: return null;
			}
		}

		public DataTemplate SelectFixedBeamTemplate(BeamCalcDataViewModel vm)
		{
			var bId = vm.BeamId;

			if (bId.TestPhase == TestPhase.Design)
				return GetTemplate(typeof(FixedBeam.Design), vm);

			if (bId.TestPhase == TestPhase.NoLoad ||
				bId.TestPhase == TestPhase.DeflectedDown ||
				bId.TestPhase == TestPhase.DeflectedDownBeforeCracks)
				switch (bId.TestDiagramNumber)
				{
					case 0: return GetTemplate(typeof(FixedBeam.DeflectedDown00), vm);
					case 1: return GetTemplate(typeof(FixedBeam.DeflectedDown01), vm);
					case 2: return GetTemplate(typeof(FixedBeam.DeflectedDown02), vm);
					case 3: return GetTemplate(typeof(FixedBeam.DeflectedDown03), vm);
				}

			if (bId.TestPhase == TestPhase.DeflectedDownCracksDevelopment)
				switch (bId.TestDiagramNumber)
				{
					case 0: return GetTemplate(typeof(FixedBeam.Cracks00), vm);
					case 1: return GetTemplate(typeof(FixedBeam.Cracks01), vm);
					case 2: return GetTemplate(typeof(FixedBeam.Cracks02), vm);
					case 3: return GetTemplate(typeof(FixedBeam.Cracks03), vm);
					case 4: return GetTemplate(typeof(FixedBeam.Cracks04), vm);
					case 5: return GetTemplate(typeof(FixedBeam.Cracks05), vm);
				}

			if (bId.TestPhase == TestPhase.Failure)
				return GetTemplate(typeof(FixedBeam.Failure), vm);

			return null;
		}

		public DataTemplate SelectReinforcedBeamTemplate(BeamCalcDataViewModel vm)
		{
			var bId = vm.BeamId;

			if (bId.TestPhase == TestPhase.Design)
				return GetTemplate(typeof(ReinforcedBeamWithFriction.Design), vm);

			if (bId.TestPhase == TestPhase.NoLoad ||
				bId.TestPhase == TestPhase.DeflectedDown ||
				bId.TestPhase == TestPhase.DeflectedDownBeforeCracks)

				switch (bId.TestDiagramNumber)
				{
					case 0: return GetTemplate(typeof(ReinforcedBeamWithFriction.DeflectedDown00), vm);
					case 1: return GetTemplate(typeof(ReinforcedBeamWithFriction.DeflectedDown01), vm);
					case 2: return GetTemplate(typeof(ReinforcedBeamWithFriction.DeflectedDown02), vm);
					case 3: return GetTemplate(typeof(ReinforcedBeamWithFriction.DeflectedDown03), vm);
				}

			if (bId.TestPhase == TestPhase.DeflectedDownCracksDevelopment)
				switch (bId.TestDiagramNumber)
				{
					case 0: return GetTemplate(typeof(ReinforcedBeamWithFriction.Cracks00), vm);
					case 1: return GetTemplate(typeof(ReinforcedBeamWithFriction.Cracks01), vm);
					case 2: return GetTemplate(typeof(ReinforcedBeamWithFriction.Cracks02), vm);
					case 3: return GetTemplate(typeof(ReinforcedBeamWithFriction.Cracks03), vm);
					case 4: return GetTemplate(typeof(ReinforcedBeamWithFriction.Cracks04), vm);
					case 5: return GetTemplate(typeof(ReinforcedBeamWithFriction.Cracks05), vm);
				}

			if (bId.TestPhase == TestPhase.Failure)
				return GetTemplate(typeof(ReinforcedBeamWithFriction.Failure), vm);

			return null;
		}

		public DataTemplate SelectPrestressedBeamTemplate(BeamCalcDataViewModel vm)
		{
			var bId = vm.BeamId;

			if (bId.TestPhase == TestPhase.Design)
				return GetTemplate(typeof(PrestressedBeamWithFriction.Design), vm);
			
			if (bId.TestPhase == TestPhase.DeflectedUp)
				switch (bId.TestDiagramNumber)
				{
					case 0: return GetTemplate(typeof(PrestressedBeamWithFriction.DeflectedUp01), vm);
					case 1: return GetTemplate(typeof(PrestressedBeamWithFriction.DeflectedUp02), vm);
					case 2: return GetTemplate(typeof(PrestressedBeamWithFriction.DeflectedUp03), vm);
					case 3: return GetTemplate(typeof(PrestressedBeamWithFriction.DeflectedUp04), vm);
					case 4: return GetTemplate(typeof(PrestressedBeamWithFriction.DeflectedUp05), vm);
				}

			if (bId.TestPhase == TestPhase.DeflectedDown || 
				bId.TestPhase == TestPhase.DeflectedDownBeforeCracks)
				switch (bId.TestDiagramNumber)
				{
					case 0: return GetTemplate(typeof(PrestressedBeamWithFriction.DeflectedDown00), vm);
					case 1: return GetTemplate(typeof(PrestressedBeamWithFriction.DeflectedDown01), vm);
					case 2: return GetTemplate(typeof(PrestressedBeamWithFriction.DeflectedDown02), vm);
					case 3: return GetTemplate(typeof(PrestressedBeamWithFriction.DeflectedDown03), vm);
					case 4: return GetTemplate(typeof(PrestressedBeamWithFriction.DeflectedDown04), vm);
				}

			if (bId.TestPhase == TestPhase.DeflectedDownCracksDevelopment)
				switch (bId.TestDiagramNumber)
				{
					case 0: return GetTemplate(typeof(PrestressedBeamWithFriction.Cracks00), vm);
					case 1: return GetTemplate(typeof(PrestressedBeamWithFriction.Cracks01), vm);
					case 2: return GetTemplate(typeof(PrestressedBeamWithFriction.Cracks02), vm);
					case 3: return GetTemplate(typeof(PrestressedBeamWithFriction.Cracks03), vm);
					case 4: return GetTemplate(typeof(PrestressedBeamWithFriction.Cracks04), vm);
				}

			if (bId.TestPhase == TestPhase.Failure)
				return GetTemplate(typeof(PrestressedBeamWithFriction.Failure), vm);

			return null;
		}

		private static DataTemplate GetTemplate(Type visualTreeType, object dataContext)
		{
			var f = new FrameworkElementFactory(visualTreeType);
			f.SetValue(FrameworkElement.DataContextProperty, dataContext);
			return new DataTemplate(visualTreeType) { VisualTree = f };
		}
	}
}