﻿using ReactiveUI;
using System.Windows;
using BeamCalculator.ViewModels;
using System.Reactive.Disposables;

namespace BeamCalculator.Views
{
	public class NavigatorBase : ReactiveUserControl<NavigatorViewModel> { }

	public partial class Navigator
	{
		public Navigator()
        {
            InitializeComponent();
	        this.WhenActivated(SetBindings);
		}

		private void LoadsOnRequestBringIntoView(object sender, RequestBringIntoViewEventArgs e) => e.Handled = true;

		private void SetBindings(CompositeDisposable d)
		{
			// Loads ComboBox:
			this.Loads.DisplayMemberPath = nameof(NavigatorItemModel.DisplayText);
			this.OneWayBind(ViewModel, vm => vm.ItemsSource, v => v.Loads.ItemsSource).DisposeWith(d);
			this.Bind(ViewModel, vm => vm.CurrentItemIndex, v => v.Loads.SelectedIndex).DisposeWith(d);

			// Commands:
			this.BindCommand(ViewModel, vm => vm.ToStart, v => v.StartButton).DisposeWith(d);
			this.BindCommand(ViewModel, vm => vm.ToPrevious, v => v.PreviousButton).DisposeWith(d);
			this.BindCommand(ViewModel, vm => vm.ToNext, v => v.NextButton).DisposeWith(d);
			this.BindCommand(ViewModel, vm => vm.ToEnd, v => v.EndButton).DisposeWith(d);
			this.BindCommand(ViewModel, vm => vm.OpenTable, v => v.TableButton).DisposeWith(d);
		}
	}
}