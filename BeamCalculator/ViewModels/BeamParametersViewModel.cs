﻿using Models;
using System;
using ReactiveUI;
using Helpers.Math;
using System.Windows;
using DynamicData.Binding;
using System.Reactive.Linq;
using ReactiveUI.Fody.Helpers;

namespace BeamCalculator.ViewModels
{
	public class BeamParametersViewModel : ReactiveObject
	{
		public BeamParametersViewModel()
		{
			BaseNumberFormatString = string.Empty;
			
			// Convert units system:
			this.WhenPropertyChanged(vm => vm.UnitsSystem)
				.Buffer(2,1).Select(b => new {previousUS = b[0], newUS = b[1]})
				.Subscribe(x => OnUnitsChanged(x.previousUS.Value, x.newUS.Value));
		}

		[Reactive] public double Width { get; set; }
		[Reactive] public double MinWidth { get; set; }
		[Reactive] public double MaxWidth { get; set; }

		[Reactive] public double Width0 { get; set; }
		[Reactive] public double MinWidth0 { get; set; }
		[Reactive] public double MaxWidth0 { get; set; }

		[Reactive] public double Width1 { get; set; }
		[Reactive] public double MinWidth1 { get; set; }
		[Reactive] public double MaxWidth1 { get; set; }

		[Reactive] public double Width2 { get; set; }
		[Reactive] public double MinWidth2 { get; set; }
		[Reactive] public double MaxWidth2 { get; set; }



		[Reactive] public double Height { get; set; }
		[Reactive] public double MinHeight { get; set; }
		[Reactive] public double MaxHeight { get; set; }

		[Reactive] public double Height0 { get; set; }
		[Reactive] public double MinHeight0 { get; set; }
		[Reactive] public double MaxHeight0 { get; set; }

		[Reactive] public double Height1 { get; set; }
		[Reactive] public double MinHeight1 { get; set; }
		[Reactive] public double MaxHeight1 { get; set; }

		[Reactive] public double Height2 { get; set; }
		[Reactive] public double MinHeight2 { get; set; }
		[Reactive] public double MaxHeight2 { get; set; }



		[Reactive] public double Length { get; set; }
		[Reactive] public double MinLength { get; set; }
		[Reactive] public double MaxLength { get; set; }



		[Reactive] public double SoleWeight { get; set; }
		[Reactive] public double MinSoleWeight { get; set; }
		[Reactive] public double MaxSoleWeight { get; set; }

		[Reactive] public double Friction { get; set; }
		[Reactive] public double MinFriction { get; set; }
		[Reactive] public double MaxFriction { get; set; }


		
		[Reactive] public double ConcretePrismStrength { get; set; }
		[Reactive] public double MinConcretePrismStrength { get; set; }
		[Reactive] public double MaxConcretePrismStrength { get; set; }


		[Reactive] public double ConcreteTensionStrength { get; set; }
		[Reactive] public double MinConcreteTensionStrength { get; set; }
		[Reactive] public double MaxConcreteTensionStrength { get; set; }


		[Reactive] public double ConcretePoissonRate { get; set; }
		[Reactive] public double MinConcretePoissonRate { get; set; }
		[Reactive] public double MaxConcretePoissonRate { get; set; }


		[Reactive] public double ConcreteElasticityModulus { get; set; }
		[Reactive] public double MinConcreteElasticityModulus { get; set; }
		[Reactive] public double MaxConcreteElasticityModulus { get; set; }

		


		[Reactive] public double ReinforcementSectionArea { get; set; }
		[Reactive] public double MinReinforcementSectionArea { get; set; }
		[Reactive] public double MaxReinforcementSectionArea { get; set; }


		[Reactive] public double ReinforcementLocationHeight { get; set; }
		[Reactive] public double MinReinforcementLocationHeight { get; set; }
		[Reactive] public double MaxReinforcementLocationHeight { get; set; }


		[Reactive] public double ReinforcementTensionStrength { get; set; }
		[Reactive] public double MinReinforcementTensionStrength { get; set; }
		[Reactive] public double MaxReinforcementTensionStrength { get; set; }


		[Reactive] public double ReinforcementPrestress { get; set; }
		[Reactive] public double MinReinforcementPrestress { get; set; }
		[Reactive] public double MaxReinforcementPrestress { get; set; }


		[Reactive] public double ReinforcementElasticityModulus { get; set; }
		[Reactive] public double MinReinforcementElasticityModulus { get; set; }
		[Reactive] public double MaxReinforcementElasticityModulus { get; set; }


		[Reactive] public string SmallNumberFormatString { get; set; }
		[Reactive] public string LargeNumberFormatString { get; set; }
		[Reactive] public string BaseNumberFormatString { get; set; }
		[Reactive] public MeasureUnitsSystem UnitsSystem { get; set; }

		

		public Visibility ShowFrictionParameter { get; set; }

		public Visibility ShowStandardSizeParameters { get; set; }
		public Visibility ShowExtendedSizeParameters { get; set; }

		public Visibility ShowReinforcementPrestress { get; set; }
		public Visibility ShowBaseReinforcementParameters { get; set; }
		

		#region PRIVATE

		private void OnUnitsChanged(MeasureUnitsSystem oldUs, MeasureUnitsSystem newUs)
		{
			if (newUs == MeasureUnitsSystem.None || oldUs == MeasureUnitsSystem.None || oldUs == newUs) return;

			Width = Algorithms.ConvertLengthValue(Width, oldUs, newUs);
			MinWidth = Algorithms.ConvertLengthValue(MinWidth, oldUs, newUs);
			MaxWidth = Algorithms.ConvertLengthValue(MaxWidth, oldUs, newUs);

			Width0 = Algorithms.ConvertLengthValue(Width0, oldUs, newUs);
			MinWidth0 = Algorithms.ConvertLengthValue(MinWidth0, oldUs, newUs);
			MaxWidth0 = Algorithms.ConvertLengthValue(MaxWidth0, oldUs, newUs);

			Width1 = Algorithms.ConvertLengthValue(Width1, oldUs, newUs);
			MinWidth1 = Algorithms.ConvertLengthValue(MinWidth1, oldUs, newUs);
			MaxWidth1 = Algorithms.ConvertLengthValue(MaxWidth1, oldUs, newUs);

			Width2 = Algorithms.ConvertLengthValue(Width2, oldUs, newUs);
			MinWidth2 = Algorithms.ConvertLengthValue(MinWidth2, oldUs, newUs);
			MaxWidth2 = Algorithms.ConvertLengthValue(MaxWidth2, oldUs, newUs);



			Height = Algorithms.ConvertLengthValue(Height, oldUs, newUs);
			MinHeight = Algorithms.ConvertLengthValue(MinHeight, oldUs, newUs);
			MaxHeight = Algorithms.ConvertLengthValue(MaxHeight, oldUs, newUs);

			Height0 = Algorithms.ConvertLengthValue(Height0, oldUs, newUs);
			MinHeight0 = Algorithms.ConvertLengthValue(MinHeight0, oldUs, newUs);
			MaxHeight0 = Algorithms.ConvertLengthValue(MaxHeight0, oldUs, newUs);

			Height1 = Algorithms.ConvertLengthValue(Height1, oldUs, newUs);
			MinHeight1 = Algorithms.ConvertLengthValue(MinHeight1, oldUs, newUs);
			MaxHeight1 = Algorithms.ConvertLengthValue(MaxHeight1, oldUs, newUs);

			Height2 = Algorithms.ConvertLengthValue(Height2, oldUs, newUs);
			MinHeight2 = Algorithms.ConvertLengthValue(MinHeight2, oldUs, newUs);
			MaxHeight2 = Algorithms.ConvertLengthValue(MaxHeight2, oldUs, newUs);


			
			Length = Algorithms.ConvertLengthValue(Length, oldUs, newUs);
			MinLength = Algorithms.ConvertLengthValue(MinLength, oldUs, newUs);
			MaxLength = Algorithms.ConvertLengthValue(MaxLength, oldUs, newUs);
			

			SoleWeight = Algorithms.ConvertLoadValue(SoleWeight, oldUs, newUs);
			MinSoleWeight = Algorithms.ConvertLoadValue(MinSoleWeight, oldUs, newUs);
			MaxSoleWeight = Algorithms.ConvertLoadValue(MaxSoleWeight, oldUs, newUs);



			ConcretePrismStrength = Algorithms.ConvertStressValue(ConcretePrismStrength, oldUs, newUs);
			MinConcretePrismStrength = Algorithms.ConvertStressValue(MinConcretePrismStrength, oldUs, newUs);
			MaxConcretePrismStrength = Algorithms.ConvertStressValue(MaxConcretePrismStrength, oldUs, newUs);

			ConcreteTensionStrength = Algorithms.ConvertStressValue(ConcreteTensionStrength, oldUs, newUs);
			MinConcreteTensionStrength = Algorithms.ConvertStressValue(MinConcreteTensionStrength, oldUs, newUs);
			MaxConcreteTensionStrength = Algorithms.ConvertStressValue(MaxConcreteTensionStrength, oldUs, newUs);

			ConcreteElasticityModulus = Algorithms.ConvertStressValue(ConcreteElasticityModulus, oldUs, newUs);
			MinConcreteElasticityModulus = Algorithms.ConvertStressValue(MinConcreteElasticityModulus, oldUs, newUs);
			MaxConcreteElasticityModulus = Algorithms.ConvertStressValue(MaxConcreteElasticityModulus, oldUs, newUs);



			ReinforcementSectionArea = Algorithms.ConvertAreaValue(ReinforcementSectionArea, oldUs, newUs);
			MinReinforcementSectionArea = Algorithms.ConvertAreaValue(MinReinforcementSectionArea, oldUs, newUs);
			MaxReinforcementSectionArea = Algorithms.ConvertAreaValue(MaxReinforcementSectionArea, oldUs, newUs);

			ReinforcementLocationHeight = Algorithms.ConvertLengthValue(ReinforcementLocationHeight, oldUs, newUs);
			MinReinforcementLocationHeight = Algorithms.ConvertLengthValue(MinReinforcementLocationHeight, oldUs, newUs);
			MaxReinforcementLocationHeight = Algorithms.ConvertLengthValue(MaxReinforcementLocationHeight, oldUs, newUs);

			ReinforcementTensionStrength = Algorithms.ConvertStressValue(ReinforcementTensionStrength, oldUs, newUs);
			MinReinforcementTensionStrength = Algorithms.ConvertStressValue(MinReinforcementTensionStrength, oldUs, newUs);
			MaxReinforcementTensionStrength = Algorithms.ConvertStressValue(MaxReinforcementTensionStrength, oldUs, newUs);

			ReinforcementPrestress = Algorithms.ConvertStressValue(ReinforcementPrestress, oldUs, newUs);
			MinReinforcementPrestress = Algorithms.ConvertStressValue(MinReinforcementPrestress, oldUs, newUs);
			MaxReinforcementPrestress = Algorithms.ConvertStressValue(MaxReinforcementPrestress, oldUs, newUs);

			ReinforcementElasticityModulus = Algorithms.ConvertStressValue(ReinforcementElasticityModulus, oldUs, newUs);
			MinReinforcementElasticityModulus = Algorithms.ConvertStressValue(MinReinforcementElasticityModulus, oldUs, newUs);
			MaxReinforcementElasticityModulus = Algorithms.ConvertStressValue(MaxReinforcementElasticityModulus, oldUs, newUs);
		}
		
		#endregion PRIVATE
	}
}