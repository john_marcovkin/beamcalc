﻿using System;
using ReactiveUI;
using System.Windows;
using DynamicData.Binding;
using System.Windows.Media;
using ReactiveUI.Fody.Helpers;


namespace BeamCalculator.ViewModels
{
	public class NumberItemModelBase : ReactiveObject
	{
		public NumberItemModelBase(double? value)
		{
			Value = value;
			NumberFormatString = string.Empty;

			this.WhenPropertyChanged(vm => vm.Value).Subscribe(v => SetText());
			this.WhenPropertyChanged(vm => vm.AdditionalText).Subscribe(v => SetText());
			this.WhenPropertyChanged(vm => vm.NumberFormatString).Subscribe(v => SetText());
		}

		[Reactive] public double? Value { get; set; }
		[Reactive] public string NumberFormatString { get; set; }
		[Reactive] public string AdditionalText { get; set; }
		[Reactive] public string DisplayText { get; set; }

		protected void SetText() => DisplayText = Value != null
			? Value.Value.ToString(NumberFormatString) + AdditionalText
			: "-" + AdditionalText;
	}
}
