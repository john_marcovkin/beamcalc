﻿using Models;

namespace BeamCalculator.ViewModels
{
	public class NavigatorItemModel : NumberItemModelBase
	{
		public NavigatorItemModel() : this(0, null, TestPhase.None) { }

		public NavigatorItemModel(int id, double value) : this(id, value, TestPhase.None) { }

		public NavigatorItemModel(int id, double? value, TestPhase phase) : base(value)
		{
			Id = id;
			TestPhase = phase;
		}

		public int Id { get; set; }
		public TestPhase TestPhase { get; set; }
	}
}