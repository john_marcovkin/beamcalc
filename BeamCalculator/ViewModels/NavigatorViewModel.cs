﻿using System;
using Models;
using ReactiveUI;
using System.Linq;
using Helpers.Math;
using PropertyChanged;
using System.Reactive;
using DynamicData.Binding;
using System.Reactive.Linq;
using ReactiveUI.Fody.Helpers;
using System.Collections.ObjectModel;

namespace BeamCalculator.ViewModels
{
	/// <summary>The model for the Navigator view control.</summary>
	public class NavigatorViewModel : ReactiveObject
    {
	    public NavigatorViewModel()
	    {
			InitializeProperties();
			InitializeCommands();
	    }

		
	    [Reactive] public int CurrentItemIndex { get; set; }
		[Reactive] public NavigatorItemModel CurrentItem { get; private set; }

	    [Reactive] public ObservableCollection<NavigatorItemModel> ItemsSource { get; set; }

		[Reactive] public string NumberFormatString { get; set; }
		[Reactive] public MeasureUnitsSystem UnitsSystem { get; set; }
		
		public ReactiveCommand<Unit, Unit> ToStart { get; private set; }
		public ReactiveCommand<Unit, Unit> ToPrevious { get; private set; }
		public ReactiveCommand<Unit, Unit> ToNext { get; private set; }
		public ReactiveCommand<Unit, Unit> ToEnd { get; private set; }
		public ReactiveCommand<Unit, Unit> OpenTable { get; private set; }


		#region PRIVATE
		
	    private void InitializeProperties()
	    {
		    // First DATA-value (the element at 0 index is not a data but beam design):
		    this.WhenPropertyChanged(m => m.ItemsSource, false)
				.Subscribe(m => RefreshCanExecuteStatus());

			this.WhenPropertyChanged(m => m.NumberFormatString)
			    .Subscribe(m => NumberFormatString = m.Value ?? string.Empty);

			// Converts load values to the new units system:
			this.WhenPropertyChanged(vm => vm.UnitsSystem)
			    .Buffer(2, 1).Select(b => new { oldUS = b[0], newUS = b[1] })
			    .Subscribe(p => OnUnitsChanged(p.oldUS.Value, p.newUS.Value));

			// Selected item changed:
		    this.WhenPropertyChanged(vm => vm.CurrentItemIndex)
			    .Buffer(2, 1).Select(b => new {oldId = b[0], newId = b[1]})
			    .Subscribe(p => OnCurrentIndexChanged(p.oldId.Value, p.newId.Value));
	    }

	    private void OnCurrentIndexChanged(int oldId, int newId)
	    {
		    if (newId < 0 && oldId > 0)
		    {
			    CurrentItemIndex = oldId;
				return;
		    }

		    SetCurrentItem();
		    RefreshCanExecuteStatus();
		}

	    private void InitializeCommands()
	    {
		    var canExecute = this.WhenAnyValue(n => n.ReverseNavigationEnabled);
			ToStart = ReactiveCommand.Create(StartExecute, canExecute);
			ToPrevious = ReactiveCommand.Create(PreviousExecute, canExecute);

		    canExecute = this.WhenAnyValue(n => n.ForwardNavigationEnabled);
			ToNext = ReactiveCommand.Create(NextExecute, canExecute);
			ToEnd = ReactiveCommand.Create(EndExecute, canExecute);

		    canExecute = this.WhenAnyValue(n => n.TableCommandEnabled);
			OpenTable = ReactiveCommand.Create(TableExecute, canExecute);
	    }
		
	    //// First DATA-value (the element at 0 is not a data but design draft):
		private bool ReverseNavigationEnabled => ItemsSource != null && CurrentItemIndex > 0;
	    private bool ForwardNavigationEnabled => ItemsSource != null && ItemsSource.Count > 1 && CurrentItemIndex < ItemsSource.Count - 1;
	    private bool TableCommandEnabled => ItemsSource != null && ItemsSource.Count > 1;

	    private void RefreshCanExecuteStatus()
	    {
		    this.RaisePropertyChanged(nameof(ReverseNavigationEnabled));
		    this.RaisePropertyChanged(nameof(ForwardNavigationEnabled));
			// TODO : table button can execute.
	    }

	    // Button commands actions:
	    private void StartExecute()
	    {
		    CurrentItemIndex = 0;
			SetCurrentItem();
		    RefreshCanExecuteStatus();
	    }

	    private void PreviousExecute()
	    {
		    CurrentItemIndex--;
		    SetCurrentItem();
		    RefreshCanExecuteStatus();
		}

	    private void NextExecute()
	    {
		    CurrentItemIndex++;
		    SetCurrentItem();
		    RefreshCanExecuteStatus();
		}

	    private void EndExecute()
	    {
		    CurrentItemIndex = ItemsSource.Count - 1;
		    SetCurrentItem();
		    RefreshCanExecuteStatus();
		}

	    private void TableExecute()
	    {
		    /* TODO : the Table-button is collapsed */ 
			throw new NotImplementedException();
	    }

	    private void SetCurrentItem()
	    {
		    CurrentItem = ItemsSource?.ElementAtOrDefault(CurrentItemIndex);
	    }

	    private void OnUnitsChanged(MeasureUnitsSystem oldUs, MeasureUnitsSystem newUs)
	    {
		    if (newUs == MeasureUnitsSystem.None || oldUs == MeasureUnitsSystem.None || oldUs == newUs || ItemsSource == null)
				return;

			foreach (var i in ItemsSource)
				if (i.Value != null)
					i.Value = Algorithms.ConvertLoadValue(i.Value.Value, oldUs, newUs);
	    }

		#endregion PRIVATE
	}
}