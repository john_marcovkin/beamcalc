﻿using Models;
using System;
using ReactiveUI;
using System.Linq;
using Helpers.Math;
using System.Reactive;
using DynamicData.Binding;
using System.Reactive.Linq;
using ReactiveUI.Fody.Helpers;
using Services.Infrastructure;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using BeamCalculator.Infrastructure;
using System.ComponentModel.Composition;
using BeamCalculator.Infrastructure.BeamParametersFactories;

namespace BeamCalculator.ViewModels
{
	public class MainWindowViewModel : ReactiveObject
	{
		// Dependencies:
		[Import] public ICalculationServiceFactory CalculationServiceFactory { get; set; }
		[Import] public DataRepository DataRepository { get; set; }

		
		// Public properties (for binding):
		[Reactive] public MeasureUnitsSystem UnitsSystem { get; set; }
		[Reactive] public NavigatorViewModel NavigatorContext { get; set; }
		[Reactive] public BeamCalcDataViewModel BeamViewerContext { get; set; }
		[Reactive] public BeamParametersViewModel BeamParametersContext { get; set; }

		public IEnumerable<BeamMenuItemModel> BeamMenuItems { get; private set; }
		[Reactive] public BeamMenuItemModel SelectedMenuItem { get; set; }
		[Reactive] public NumberItemModelBase LoadStepValue { get; set; }
		[Reactive] public IObservableCollection<NumberItemModelBase> LoadSteps { get; private set; }
		[Reactive] private Dictionary<int, BeamCalcDataViewModel> CalcData { get; set; }

		public Interaction<Unit, Unit> NotifyCalculationError { get; private set; }

		public IBeamParametersFactory BeamParametersFactory { get; set; } = 
			new DefaultBeamParametersFactory();
		

		// Commands:
		public ReactiveCommand<BeamMenuItemModel, Unit> OnMenuItemChangedCommand { get; private set; }
		public ReactiveCommand<Unit, Unit> CalculateCommand { get; private set; }
		public ReactiveCommand<Unit, Unit> ClearCommand { get; private set; }


		private BeamType _CurrentBeam;
		private Dictionary<BeamType, BeamViewerModel> _ModelsCache;
		private const string _BaseNumberFormatString = "#,##0.###";

		public void Initialize()
		{
			NotifyCalculationError = new Interaction<Unit, Unit>();

			// Menu:
			var menuItems = GetMenuItems();
			BeamMenuItems = menuItems;
			SelectedMenuItem = menuItems.FirstOrDefault();

			// Load default models:
			_ModelsCache = new Dictionary<BeamType, BeamViewerModel>();
			LoadBeamModel(SelectedMenuItem.BeamType);

			// Load steps combo:
			var loads = DataRepository.SiLoadSteps
				.Select(ls => new NumberItemModelBase(ls){NumberFormatString = _BaseNumberFormatString});

			LoadSteps = new ObservableCollectionExtended<NumberItemModelBase>(loads);
			LoadStepValue = LoadSteps.FirstOrDefault();

			// Navigator, parameters, beam viewer, units system:
			this.WhenPropertyChanged(vm => vm.NavigatorContext.CurrentItem)
				.Subscribe(p =>
				{
					CalcData.TryGetValue(p.Value.Id, out var vm);
					BeamViewerContext = vm;
				});

			this.WhenPropertyChanged(vm => vm.CalcData)
				.Subscribe(x => NavigatorContext.ItemsSource = GetNavigatorItems(x.Value));
			
			// Makes sure that the units system of the new BeamCalcDataModel
			// is equal to the global US (ex: when user navigates trough the
			// calculation results). They can differ because changing the
			// global US affects only the current BeamCalcDataModel to
			// improve performance:
			this.WhenPropertyChanged(vm => vm.BeamViewerContext)
				.Subscribe(x => x.Value.UnitsSystem = UnitsSystem);
			
			this.WhenPropertyChanged(vm => vm.UnitsSystem)
				.Buffer(2, 1).Select(b => new { oldUS = b[0], newUS = b[1] })
				.Subscribe(p => OnUnitsChanged(p.oldUS.Value, p.newUS.Value));

			// Commands:
			ClearCommand = ReactiveCommand.Create(Clear);
			CalculateCommand = ReactiveCommand.Create(OnCalculate);
			OnMenuItemChangedCommand = ReactiveCommand.Create<BeamMenuItemModel>(OnMenuItemChangedExecute);
		}

		public ObservableCollection<NavigatorItemModel> GetNavigatorItems(IDictionary<int, BeamCalcDataViewModel> beamCalcData)
		{
			var items = new List<NavigatorItemModel>(beamCalcData.Count);

			foreach (var i in beamCalcData.OrderBy(b => b.Key))
			{
				var item = new NavigatorItemModel(i.Key, i.Value.Load, i.Value.BeamId.TestPhase)
					{NumberFormatString = _BaseNumberFormatString};

				switch (i.Value.BeamId.TestPhase)
				{
					case TestPhase.DeflectedDownBeforeCracks: item.AdditionalText = " - предел трещиностойкости"; break;
					case TestPhase.Failure: item.AdditionalText = " - балка разрушилась"; break;
				}

				items.Add(item);
			}
			
			return new ObservableCollection<NavigatorItemModel>(items);
		}

		private void OnMenuItemChangedExecute(BeamMenuItemModel arg) => LoadBeamModel(arg.BeamType);

		private static IEnumerable<BeamMenuItemModel> GetMenuItems()
		{
			var tooltips = new Dictionary<BeamType, string>
			{
				//{BeamType.DeepBeam, "Балка-стенка" },
				//{BeamType.ReinforcedBeam, "Балка с армированием" },

				{BeamType.FixedBeam, "Балка с защемлением" },
				{BeamType.PrestressedBeam, "Балка с армированием" }
			};

			var items = new List<BeamMenuItemModel>();
			foreach (BeamType beam in Enum.GetValues(typeof(BeamType)))
			{
				if (beam == BeamType.None || 
					beam == BeamType.DeepBeam || 
					beam == BeamType.ReinforcedBeam) continue;

				var item = new BeamMenuItemModel
				{
					BeamType = beam,
					BeamIconKey = $"Icons.{beam.ToString()}",
					TooltipText = tooltips[beam]
				};

				items.Add(item);
			}

			return items;
		}

		private void LoadBeamModel(BeamType type, bool cacheCurrent = true)
		{
			if(cacheCurrent) CacheCurrentViewData();
			
			// Load another beam view models:
			_CurrentBeam = type;
			var model = GetViewerModel(type);

			// To avoid unexpected changes of models while they are loading:
			using (this.SuppressChangeNotifications())
			{
				UnitsSystem = model.UnitsSystem;
				CalcData = model.BeamViewerData;
				BeamViewerContext = model.BeamData;
				NavigatorContext = model.Navigator;
				BeamParametersContext = model.BeamParameters;
			}
			
			this.RaisePropertyChanged(nameof(UnitsSystem));
			this.RaisePropertyChanged(nameof(CalcData));
			this.RaisePropertyChanged(nameof(BeamViewerContext));
			this.RaisePropertyChanged(nameof(NavigatorContext));
			this.RaisePropertyChanged(nameof(BeamParametersContext));
		}

		private BeamViewerModel GetViewerModel(BeamType type)
		{
			return _ModelsCache.TryGetValue(_CurrentBeam, out var model)
				? model
				: CreateBeamViewerModel(type);
		}

		private void CacheCurrentViewData()
		{
			if (!_ModelsCache.TryGetValue(_CurrentBeam, out var model)) return;
			model.UnitsSystem = UnitsSystem;
			model.BeamData = BeamViewerContext;
			model.Navigator = NavigatorContext;
			model.BeamViewerData = CalcData;
			model.BeamParameters = BeamParametersContext;
		}

		private BeamViewerModel CreateBeamViewerModel(BeamType type)
		{
			var parameters = BeamParametersFactory.GetBeamParameters(type, MeasureUnitsSystem.Si);

			var beamData = new BeamCalcDataViewModel
			{
				BeamId = new BeamId(type, TestPhase.Design, 0),
				Load = null,
				Length = parameters.Length,
				Width = parameters.Width,
				Width0 = parameters.Width0,
				Width1 = parameters.Width1,
				Width2 = parameters.Width2,
				Height = parameters.Height,
				Height0 = parameters.Height0,
				Height1 = parameters.Height1,
				Height2 = parameters.Height2,
				
				ReinforcementSectionArea = parameters.ReinforcementSectionArea,
				ReinforcementLocationHeight = parameters.ReinforcementLocationHeight,
				UnitsSystem = parameters.UnitsSystem
			};
			
			// Synchronize Beam parameters and Design view. 
			// This must work only on a single property change.
			// This must not work when the Global units system changes
			// to avoid cycling changes (setting UnitsSystem property
			// on view models automatically convert other properties):
			parameters.WhenAny(p => p.Length, p => p.Sender)
				.Where(p => p.UnitsSystem == beamData.UnitsSystem)
				.Subscribe(p => beamData.Length = p.Length);

			parameters.WhenAny(p => p.Height, p => p.Sender)
				.Where(p => p.UnitsSystem == beamData.UnitsSystem)
				.Subscribe(p => beamData.Height = p.Height);

			parameters.WhenAny(p => p.Height0, p => p.Sender)
				.Where(p => p.UnitsSystem == beamData.UnitsSystem)
				.Subscribe(p => beamData.Height0 = p.Height0);

			parameters.WhenAny(p => p.Height1, p => p.Sender)
				.Where(p => p.UnitsSystem == beamData.UnitsSystem)
				.Subscribe(p => beamData.Height1 = p.Height1);

			parameters.WhenAny(p => p.Height2, p => p.Sender)
				.Where(p => p.UnitsSystem == beamData.UnitsSystem)
				.Subscribe(p => beamData.Height2 = p.Height2);

			parameters.WhenAny(p => p.Width, p => p.Sender)
				.Where(p => p.UnitsSystem == beamData.UnitsSystem)
				.Subscribe(p => beamData.Width = p.Width);

			parameters.WhenAny(p => p.Width0, p => p.Sender)
				.Where(p => p.UnitsSystem == beamData.UnitsSystem)
				.Subscribe(p => beamData.Width0 = p.Width0);

			parameters.WhenAny(p => p.Width1, p => p.Sender)
				.Where(p => p.UnitsSystem == beamData.UnitsSystem)
				.Subscribe(p => beamData.Width1 = p.Width1);

			parameters.WhenAny(p => p.Width2, p => p.Sender)
				.Where(p => p.UnitsSystem == beamData.UnitsSystem)
				.Subscribe(p => beamData.Width2 = p.Width2);
			
			parameters.WhenAny(p => p.ReinforcementSectionArea, p => p.Sender)
				.Where(p => p.UnitsSystem == beamData.UnitsSystem)
				.Subscribe(p => beamData.ReinforcementSectionArea = p.ReinforcementSectionArea);

			parameters.WhenAny(p => p.ReinforcementLocationHeight, p => p.Sender)
				.Where(p => p.UnitsSystem == beamData.UnitsSystem)
				.Subscribe(p => beamData.ReinforcementLocationHeight = p.ReinforcementLocationHeight);


			var navigator = new NavigatorViewModel
			{
				ItemsSource = new ObservableCollection<NavigatorItemModel>
				{ new NavigatorItemModel {NumberFormatString = _BaseNumberFormatString} },
				UnitsSystem = parameters.UnitsSystem,
				CurrentItemIndex = 0
			};

			// This object stores beam parameters to restore them when
			// user selects this beam type from the main left menu again:
			var model = new BeamViewerModel
			{
				BeamType = type,
				BeamData = beamData,
				Navigator = navigator,
				BeamParameters = parameters,
				UnitsSystem = parameters.UnitsSystem,
				Calculator = CalculationServiceFactory.GetCalculationService(type),
				BeamViewerData = new Dictionary<int, BeamCalcDataViewModel> { { 0, beamData} }
			};
			
			_ModelsCache.Add(type, model);
			return model;
		}

		/// <summary>User has changed units' system.</summary>
		private void OnUnitsChanged(MeasureUnitsSystem oldUs, MeasureUnitsSystem newUs)
		{
			if (oldUs == MeasureUnitsSystem.None || newUs == MeasureUnitsSystem.None || oldUs == newUs) return;

			// Convert load steps values:
			foreach (var ls in LoadSteps)
				if(ls.Value != null)
					ls.Value = Algorithms.ConvertLoadValue(ls.Value.Value, oldUs, newUs);

			// Update sub-views:
			NavigatorContext.UnitsSystem = newUs;
			BeamViewerContext.UnitsSystem = newUs;
			BeamParametersContext.UnitsSystem = newUs;
		}

		/// <summary>Remove any beam parameters and calculation data.</summary>
		private void Clear()
		{
			_ModelsCache.Remove(_CurrentBeam);
			LoadBeamModel(_CurrentBeam, false);
		}

		/// <summary>Calculate button is clicked.</summary>
		private async void OnCalculate()
		{
			if(LoadStepValue?.Value == null) return;

			var parameters = new BeamInputParameters
			{
				BeamType = _CurrentBeam,
				UnitsSystem = UnitsSystem,
				OutputLoadStep = LoadStepValue.Value.Value,

				Width = BeamParametersContext.Width,
				Width0 = BeamParametersContext.Width0,
				Width1 = BeamParametersContext.Width1,
				Width2 = BeamParametersContext.Width2,

				Height = BeamParametersContext.Height,
				Height0 = BeamParametersContext.Height0,
				Height1 = BeamParametersContext.Height1,
				Height2 = BeamParametersContext.Height2,

				Length = BeamParametersContext.Length,

				Friction = BeamParametersContext.Friction,
				BeamWeight = BeamParametersContext.SoleWeight,

				ConcretePoissonRate = BeamParametersContext.ConcretePoissonRate,
				ConcretePrismStrength = BeamParametersContext.ConcretePrismStrength,
				ConcreteTensionStrength = BeamParametersContext.ConcreteTensionStrength,
				ConcreteElasticityModulus = BeamParametersContext.ConcreteElasticityModulus,

				ReinforcementPrestress = BeamParametersContext.ReinforcementPrestress,
				ReinforcementSectionArea = BeamParametersContext.ReinforcementSectionArea,
				ReinforcementLocationHeight = BeamParametersContext.ReinforcementLocationHeight,
				ReinforcementTensionStrength = BeamParametersContext.ReinforcementTensionStrength,
				ReinforcementElasticityModulus = BeamParametersContext.ReinforcementElasticityModulus,
			};

			var calculator = CalculationServiceFactory.GetCalculationService(_CurrentBeam);
			var beamDesign = CalcData[0];

			try
			{
				var data = calculator.Calculate(parameters);
				data.Add(0, beamDesign);
				CalcData = data;
				if (data.Count > 1) NavigatorContext.CurrentItemIndex = 1;
			}
			catch
			{
				await NotifyCalculationError.Handle(Unit.Default);
			}
		}
	}
}