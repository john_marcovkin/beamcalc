﻿using System.Collections.Generic;
using BeamCalculator.Infrastructure;
using Models;

namespace BeamCalculator.ViewModels
{
    public class BeamViewerModel
    {
	    public  BeamType BeamType { get; set; }
		public MeasureUnitsSystem UnitsSystem { get; set; }
		
		public ICalculationService Calculator { get; set; }
		public NavigatorViewModel Navigator { get; set; }
	    public BeamCalcDataViewModel BeamData { get; set; }
		public BeamParametersViewModel BeamParameters { get; set; }

		/// <summary>Load values vs corresponding calculated values.</summary>
	    public Dictionary<int, BeamCalcDataViewModel> BeamViewerData { get; set; }
	}
}