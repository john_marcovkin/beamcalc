﻿using Models;

namespace BeamCalculator.ViewModels
{
	public class BeamMenuItemModel
	{
		public BeamType BeamType { get; set; }
		public string BeamIconKey { get; set; }
		public string TooltipText { get; set; }
	}
}