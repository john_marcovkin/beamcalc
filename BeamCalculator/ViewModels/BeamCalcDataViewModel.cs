﻿using Models;
using System;
using ReactiveUI;
using Helpers.Math;
using DynamicData.Binding;
using System.Reactive.Linq;
using ReactiveUI.Fody.Helpers;

namespace BeamCalculator.ViewModels
{
	public class BeamCalcDataViewModel : ReactiveObject
	{
		public BeamCalcDataViewModel()
		{
			NumbersFormatString = string.Empty;

			this.WhenPropertyChanged(vm => vm.UnitsSystem)
				.Buffer(2, 1).Select(b => new { previousUS = b[0], newUS = b[1] })
				.Subscribe(x => OnUnitsChanged(x.previousUS.Value, x.newUS.Value));
		}

		public BeamId BeamId { get; set; }

		[Reactive] public double Width { get; set; }
		[Reactive] public double Width0 { get; set; }
		[Reactive] public double Width1 { get; set; }
		[Reactive] public double Width2 { get; set; }

		[Reactive] public double Height { get; set; }
		[Reactive] public double Height0 { get; set; }
		[Reactive] public double Height1 { get; set; }
		[Reactive] public double Height2 { get; set; }

		[Reactive] public double Length { get; set; }

		[Reactive] public double? Load { get; set; }
		[Reactive] public double Deflection { get; set; }

		[Reactive] public double ConcreteTension { get; set; }
		[Reactive] public double ConcreteCompression { get; set; }

		[Reactive] public double ReinforcementTension { get; set; }
		[Reactive] public double ReinforcementSectionArea { get; set; }
		[Reactive] public double ReinforcementLocationHeight { get; set; }

		[Reactive] public double CracksWidth { get; set; }
		[Reactive] public double CracksHeight { get; set; }

		[Reactive] public string NumbersFormatString { get; set; }
		[Reactive] public MeasureUnitsSystem UnitsSystem { get; set; }

		private void OnUnitsChanged(MeasureUnitsSystem oldUs, MeasureUnitsSystem newUs)
		{
			if (newUs == MeasureUnitsSystem.None || oldUs == MeasureUnitsSystem.None || oldUs == newUs) return;

			Width = Algorithms.ConvertLengthValue(Width, oldUs, newUs);
			Width0 = Algorithms.ConvertLengthValue(Width0, oldUs, newUs);
			Width1 = Algorithms.ConvertLengthValue(Width1, oldUs, newUs);
			Width2 = Algorithms.ConvertLengthValue(Width2, oldUs, newUs);
			Height = Algorithms.ConvertLengthValue(Height, oldUs, newUs);
			Height0 = Algorithms.ConvertLengthValue(Height0, oldUs, newUs);
			Height1 = Algorithms.ConvertLengthValue(Height1, oldUs, newUs);
			Height2 = Algorithms.ConvertLengthValue(Height2, oldUs, newUs);
			Length = Algorithms.ConvertLengthValue(Length, oldUs, newUs);

			if(Load != null) Load = Algorithms.ConvertLoadValue(Load.Value, oldUs, newUs);
			Deflection = Algorithms.ConvertLengthValue(Deflection, oldUs, newUs);

			ConcreteTension = Algorithms.ConvertStressValue(ConcreteTension, oldUs, newUs);
			ConcreteCompression = Algorithms.ConvertStressValue(ConcreteCompression, oldUs, newUs);

			ReinforcementTension = Algorithms.ConvertStressValue(ReinforcementTension, oldUs, newUs);
			ReinforcementSectionArea = Algorithms.ConvertAreaValue(ReinforcementSectionArea, oldUs, newUs);
			ReinforcementLocationHeight = Algorithms.ConvertLengthValue(ReinforcementLocationHeight, oldUs, newUs);

			CracksWidth = Algorithms.ConvertLengthValue(CracksWidth, oldUs, newUs);
			CracksHeight = Algorithms.ConvertLengthValue(CracksHeight, oldUs, newUs);
		}
	}
}