﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Helpers.Math;

namespace BeamCalculator.ValueConverters
{
    public class GetMaxDoubleValueConverter: IMultiValueConverter
	{
	    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
	    {
		    var v = values.Select(o => (double)o).ToList();
		    return Algorithms.Max(v);
	    }

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
			=> throw new NotImplementedException();
	}
}