﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Globalization;

namespace BeamCalculator.ValueConverters
{
	public class KeyToResourceConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is string key)) return null;

			return parameter is FrameworkElement element
				? element.TryFindResource(key)
				: Application.Current.TryFindResource(key);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
