﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace BeamCalculator.ValueConverters
{
	public class FloatToStringConverter : IValueConverter
	{
		public char GroupsSeparator
		{
			get => _GroupSeparator;
			set
			{
				_GroupSeparator = value;
				_NumberFormat.NumberGroupSeparator = new string(_GroupSeparator, 1);
			}
		}
		public char DecimalSeparator
		{
			get => _DecimalSeparator;
			set
			{
				_DecimalSeparator = value;
				_NumberFormat.NumberDecimalSeparator = new string(_DecimalSeparator, 1);
			}
		}

		private char _GroupSeparator = ' ';
		private char _DecimalSeparator = '.';
		private byte _MaximumNumberOfDigits = 1;
		private byte _MaximumNumberOfExponentDigits = 1;
		private readonly NumberFormatInfo _NumberFormat = new NumberFormatInfo
		{
			NumberGroupSeparator = " ",
			NumberDecimalSeparator = "."
		};

		public byte MaximumNumberOfDigits
		{
			get => _MaximumNumberOfDigits;
			set
			{
				if (value < 1)
				{
					_MaximumNumberOfDigits = 1;
					return;
				}

				if (value > 15)
				{
					_MaximumNumberOfDigits = 15;
					return;
				}

				_MaximumNumberOfDigits = value;
			}
		}

		public byte MaximumNumberOfExponentDigits
		{
			get => _MaximumNumberOfExponentDigits;
			set
			{
				if (value < 1)
				{
					_MaximumNumberOfExponentDigits = 1;
					return;
				}

				if (value > 15)
				{
					_MaximumNumberOfExponentDigits = 15;
					return;
				}

				_MaximumNumberOfExponentDigits = value;
			}
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (targetType != typeof(string)) return null;

			switch (value)
			{
				case double d: return Convert(d);
				case float f: return Convert(f);
				default: return null;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is string s)) return 0;

			if (targetType == typeof(double))
				return double.TryParse(s, NumberStyles.Any, _NumberFormat, out double d) ? d : 0D;

			if (targetType == typeof(float))
				return float.TryParse(s, NumberStyles.Any, _NumberFormat, out float d) ? d : 0F;

			return 0;
		}

		public string Convert(double value)
		{
			if (double.IsNaN(value) || double.IsInfinity(value))
				return string.Empty;

			var abs = Math.Abs(value);
			if (abs <= double.Epsilon)
				return "0";

			var p = (int)Math.Log10(abs);
			var sign = value < 0 ? "-" : string.Empty;

			const string format = "#,##0.###############";
			if (p > _MaximumNumberOfDigits - 1 || p < -MaximumNumberOfDigits + 1)
				return value.ToString($"E{_MaximumNumberOfExponentDigits}", _NumberFormat);
			else if(p >= 0)
				return sign + Math.Round(value, _MaximumNumberOfDigits - p - 1).ToString(format, _NumberFormat);
			else
				return sign + Math.Round(value, _MaximumNumberOfDigits).ToString(format, _NumberFormat);
		}
	}
}