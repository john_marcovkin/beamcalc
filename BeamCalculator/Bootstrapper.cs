﻿using DI;
using Splat;
using ReactiveUI;
using System.Reflection;
using BeamCalculator.ViewModels;
using BeamCalculator.Infrastructure;

namespace BeamCalculator
{
	public class Bootstrapper
	{
		public void RegisterDependencies()
		{
			var resolver = new DependencyInjector();
			new Services.Bootstrapper().RegisterDependencies(resolver);
			RegisterThisAssemblyDependencies(resolver);

			resolver.Verify();
			Locator.CurrentMutable = resolver;
		}

		private static void RegisterThisAssemblyDependencies(DependencyInjector resolver)
		{
			resolver.RegisterViewsForViewModels(Assembly.GetEntryAssembly());
			resolver.RegisterSingleton<ICalculationServiceFactory, CalculationServiceFactory>();
			resolver.Register<MainWindowViewModel>(LifeStyle.Transient);
		}
	}
}