﻿using System.Windows;

namespace BeamCalculator.Common.MarkedImageParsingService
{
	public class MarkColorInfo
	{
		public MarkColorInfo() { }

		public MarkColorInfo(DisplayElementType equation, HorizontalAlignment alignment)
		{
			Equation = equation;
			Alignment = alignment;
		}

		public DisplayElementType Equation { get; set; }
		public HorizontalAlignment Alignment { get; set; }
	}
}