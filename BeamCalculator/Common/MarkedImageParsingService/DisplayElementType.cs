﻿namespace BeamCalculator.Common.MarkedImageParsingService
{
    public enum DisplayElementType
    {
		MainImage = 0,

		Width,
		Height,
		Length,

		ConcreteTension,
		ConcreteCompression,
		ReinforcementTension,

		Friction
    }
}