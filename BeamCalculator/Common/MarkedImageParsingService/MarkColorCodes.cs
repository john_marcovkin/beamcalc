﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BeamCalculator.Common.MarkedImageParsingService
{
    public partial class MarkedImageParser
    {
	    public Dictionary<string, MarkColorInfo> MarkTypes = new Dictionary<string, MarkColorInfo>
	    {
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.Height, HorizontalAlignment.Left)},
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.Height, HorizontalAlignment.Center)},
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.Height, HorizontalAlignment.Right)},

		    {"#FF12345678", new MarkColorInfo(DisplayElementType.Length, HorizontalAlignment.Left)},
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.Length, HorizontalAlignment.Center)},
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.Length, HorizontalAlignment.Right)},

		    {"#FF12345678", new MarkColorInfo(DisplayElementType.Width, HorizontalAlignment.Left)},
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.Width, HorizontalAlignment.Center)},
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.Width, HorizontalAlignment.Right)},

		    {"#FF12345678", new MarkColorInfo(DisplayElementType.ConcreteTension, HorizontalAlignment.Left)},
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.ConcreteTension, HorizontalAlignment.Center)},
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.ConcreteTension, HorizontalAlignment.Right)},

		    {"#FF12345678", new MarkColorInfo(DisplayElementType.ConcreteCompression, HorizontalAlignment.Left)},
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.ConcreteCompression, HorizontalAlignment.Center)},
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.ConcreteCompression, HorizontalAlignment.Right)},

		    {"#FF12345678", new MarkColorInfo(DisplayElementType.ReinforcementTension, HorizontalAlignment.Left)},
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.ReinforcementTension, HorizontalAlignment.Center)},
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.ReinforcementTension, HorizontalAlignment.Right)},

		    {"#FF12345678", new MarkColorInfo(DisplayElementType.Friction, HorizontalAlignment.Left)},
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.Friction, HorizontalAlignment.Center)},
		    {"#FF12345678", new MarkColorInfo(DisplayElementType.Friction, HorizontalAlignment.Right)}
		};
    }
}