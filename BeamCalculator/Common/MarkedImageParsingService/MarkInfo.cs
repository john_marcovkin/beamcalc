﻿using System.Windows;

namespace BeamCalculator.Common.MarkedImageParsingService
{
	public class MarkInfo : MarkColorInfo
	{
		public MarkInfo() { }

		public MarkInfo(MarkColorInfo ci)
		{
			base.Alignment = ci.Alignment;
			base.Equation = ci.Equation;
		}

		/// <summary>The origin point to place the element.</summary>
		public Point Origin { get; set; }
		public double Angle { get; set; }
	}
}