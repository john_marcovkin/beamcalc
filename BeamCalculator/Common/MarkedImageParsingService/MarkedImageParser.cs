﻿using System.Windows;
using System.Windows.Media;
using System.Collections.Generic;

namespace BeamCalculator.Common.MarkedImageParsingService
{
	public partial class MarkedImageParser
	{
		public IEnumerable<MarkInfo> Parse(DrawingGroup beamImage)
		{
			var output = new List<MarkInfo>();

			return output;
		}

		private IEnumerable<MarkInfo> GetMarks(DrawingGroup group)
		{
			var output = new List<MarkInfo>();

			foreach (var child in group.Children)
			{
				if (child is DrawingGroup dg) return GetMarks(dg);
				if (child is GeometryDrawing gd
				    && gd.Geometry is RectangleGeometry rg
				    && gd.Brush is SolidColorBrush cb
				    && MarkTypes.TryGetValue(cb.ToString(), out var mt))
				{
					output.Add(new MarkInfo(mt)
					{
						Origin = new Point(rg.Rect.X + rg.Rect.Width/2, rg.Rect.Y + rg.Rect.Height/2)
					});
				}
			}

			return output;
		}

		private double GetMarkAngle(EllipseGeometry eg)
		{
			return 0;
		}
	}
}