﻿using System.Globalization;
using System.Threading;

namespace BeamCalculator
{
	public partial class App
	{
		public App()
		{
			SetDefaultNumberFormats();
			new Bootstrapper().RegisterDependencies();
		}

		private static void SetDefaultNumberFormats()
		{
			var culture = (CultureInfo)CultureInfo.InvariantCulture.Clone();
			var nFormat = (NumberFormatInfo)NumberFormatInfo.InvariantInfo.Clone();
			nFormat.NumberGroupSeparator = " ";
			nFormat.NumberDecimalSeparator = ".";
			culture.NumberFormat = nFormat;

			Thread.CurrentThread.CurrentCulture = culture;
			Thread.CurrentThread.CurrentUICulture = culture;
		}
	}
}