﻿using Models;
using System.Windows;
using BeamCalculator.ViewModels;

namespace BeamCalculator.Infrastructure.BeamParametersFactories
{
	public class ReinforcedBeamParametersFactory
	{
		public static BeamParametersViewModel GetParameters(MeasureUnitsSystem us)
		{
			var output = new BeamParametersViewModel
			{
				Length = 1,
				MinLength = 1,
				MaxLength = 100,

				Height0 = 0.1,
				MaxHeight0 = 5,
				MinHeight0 = 0.1,

				Height1 = 0,
				MinHeight1 = 0,
				MaxHeight1 = 5,

				Height2 = 0,
				MinHeight2 = 0,
				MaxHeight2 = 5,

				Width0 = 0.1,
				MaxWidth0 = 2.5,
				MinWidth0 = 0.1,

				Width1 = 0,
				MinWidth1 = 0,
				MaxWidth1 = 2.5,

				Width2 = 0,
				MinWidth2 = 0,
				MaxWidth2 = 2.5,

				SoleWeight = 0,
				MinSoleWeight = 0,
				MaxSoleWeight = 1000000,

				Friction = 0,
				MinFriction = 0,
				MaxFriction = 1,


				ConcretePoissonRate = 0.16,
				MaxConcretePoissonRate = 1,
				MinConcretePoissonRate = 0.1,

				ConcretePrismStrength = 30,
				MinConcretePrismStrength = 5,
				MaxConcretePrismStrength = 200,

				ConcreteTensionStrength = 2,
				MaxConcreteTensionStrength = 50,
				MinConcreteTensionStrength = 0.5,

				ConcreteElasticityModulus = 30000,
				MinConcreteElasticityModulus = 10000,
				MaxConcreteElasticityModulus = 200000,


				ReinforcementLocationHeight = 0.1,
				MaxReinforcementLocationHeight = 10,
				MinReinforcementLocationHeight = 0.1,

				ReinforcementElasticityModulus = 2.1e5D,
				MinReinforcementElasticityModulus = 1.0e5D, // reinforcement diameter = 8 mm.
				MaxReinforcementElasticityModulus = 1.0e6D,

				ReinforcementSectionArea = 3 * 5e-5, 
				MinReinforcementSectionArea = 5e-5, // reinforcement diameter = 8 mm.
				MaxReinforcementSectionArea = 0.1,

				ReinforcementTensionStrength = 400D,
				MinReinforcementTensionStrength = 100D,
				MaxReinforcementTensionStrength = 5000D,

				ReinforcementPrestress = 3000,
				MinReinforcementPrestress = 0,
				MaxReinforcementPrestress = 100000,

				ShowExtendedSizeParameters = Visibility.Visible,
				ShowStandardSizeParameters = Visibility.Collapsed,

				ShowReinforcementPrestress = Visibility.Collapsed,
				ShowBaseReinforcementParameters = Visibility.Visible,
				ShowFrictionParameter = Visibility.Visible,

				UnitsSystem = MeasureUnitsSystem.Si
			};
			
			// Convert values if necessary:
			output.UnitsSystem = us;
			return output;
		}
	}
}
