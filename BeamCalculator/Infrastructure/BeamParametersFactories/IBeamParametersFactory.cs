﻿using BeamCalculator.ViewModels;
using Models;

namespace BeamCalculator.Infrastructure.BeamParametersFactories
{
	public interface IBeamParametersFactory
	{
		BeamParametersViewModel GetBeamParameters(BeamType type, MeasureUnitsSystem us);
	}
}
