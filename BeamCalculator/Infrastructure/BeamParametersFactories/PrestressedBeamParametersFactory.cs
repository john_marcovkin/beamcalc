﻿using System.Windows;
using BeamCalculator.ViewModels;
using Helpers.Math;
using Models;

namespace BeamCalculator.Infrastructure.BeamParametersFactories
{
	public class PrestressedBeamParametersFactory
	{
		public static BeamParametersViewModel GetParameters(MeasureUnitsSystem us)
		{
			var parameters = ReinforcedBeamParametersFactory.GetParameters(us);
			parameters.ShowReinforcementPrestress = Visibility.Visible;

			const int maxPrestress = 10000;

			switch (us)
			{
				case MeasureUnitsSystem.Si:
					parameters.ReinforcementPrestress = 0;
					parameters.MinReinforcementPrestress = 0; 
					parameters.MaxReinforcementPrestress = maxPrestress;
					break;

				case MeasureUnitsSystem.Tech:
					parameters.ReinforcementPrestress = 0;
					parameters.MinReinforcementPrestress = 0;

					parameters.MaxReinforcementPrestress = 
						Algorithms.ConvertStressValue(maxPrestress, MeasureUnitsSystem.Si, MeasureUnitsSystem.Tech);
					break;
			}

			return parameters;
		}
	}
}