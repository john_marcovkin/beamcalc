﻿using BeamCalculator.ViewModels;
using Models;

namespace BeamCalculator.Infrastructure.BeamParametersFactories
{
	public class DefaultBeamParametersFactory : IBeamParametersFactory
	{
		public BeamParametersViewModel GetBeamParameters(BeamType type, MeasureUnitsSystem us)
		{
			switch (type)
			{
				case BeamType.FixedBeam: return FixedBeamParametersFactory.GetParameters(us);
				case BeamType.ReinforcedBeam: return ReinforcedBeamParametersFactory.GetParameters(us);
				case BeamType.PrestressedBeam: return PrestressedBeamParametersFactory.GetParameters(us);
				default: return new BeamParametersViewModel();
			}
		}
	}
}