﻿using Models;
using System.Windows;
using BeamCalculator.ViewModels;

namespace BeamCalculator.Infrastructure.BeamParametersFactories
{
	public class FixedBeamParametersFactory
	{
		public static BeamParametersViewModel GetParameters(MeasureUnitsSystem us)
		{
			var output = new BeamParametersViewModel
			{
				Width = 0.1,
				MaxWidth = 2,
				MinWidth = 0.05,

				Length = 1,
				MinLength = 1,
				MaxLength = 30,

				Height = 0.1,
				MaxHeight = 5,
				MinHeight = 0.1,

				SoleWeight = 0,
				MinSoleWeight = 0,
				MaxSoleWeight = 1000000,

				ConcretePoissonRate = 0.16,
				MaxConcretePoissonRate = 1,
				MinConcretePoissonRate = 0.1,

				ConcretePrismStrength = 30,
				MinConcretePrismStrength = 5,
				MaxConcretePrismStrength = 200,

				ConcreteTensionStrength = 2,
				MaxConcreteTensionStrength = 50,
				MinConcreteTensionStrength = 0.5,

				ConcreteElasticityModulus = 30000,
				MinConcreteElasticityModulus = 10000,
				MaxConcreteElasticityModulus = 200000,

				ShowFrictionParameter = Visibility.Collapsed,
				ShowStandardSizeParameters = Visibility.Visible,
				ShowExtendedSizeParameters = Visibility.Collapsed,
				ShowReinforcementPrestress = Visibility.Collapsed,
				ShowBaseReinforcementParameters = Visibility.Collapsed,
				UnitsSystem = MeasureUnitsSystem.Si
			};

			// Convert values if necessary:
			output.UnitsSystem = us;
			return output;
		}
	}
}