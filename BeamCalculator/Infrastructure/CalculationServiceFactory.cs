﻿using Models;
using Services.Calculators;
using System.Collections.Generic;
using BeamCalculator.Infrastructure.BeamCalcDataMappers;

namespace BeamCalculator.Infrastructure
{
	public class CalculationServiceFactory : ICalculationServiceFactory
	{
		private readonly Dictionary<BeamType, ICalculationService> _Cache = new Dictionary<BeamType, ICalculationService>();

		public ICalculationService GetCalculationService(BeamType type)
		{
			if (_Cache.TryGetValue(type, out var service)) return service;

			var calculator = GetCalculator(type);
			var mapper = GetMapper(type);
			service = new CalculationService(calculator, mapper);

			_Cache.Add(type, service);
			return service;
		}

		private static IBeamCalculator GetCalculator(BeamType type)
		{
			switch (type)
			{
				case BeamType.DeepBeam: return new Services.Calculators.DeepBeam.Calculator();
				case BeamType.FixedBeam: return new Services.Calculators.FixedBeam.Calculator();
				case BeamType.ReinforcedBeam: return new Services.Calculators.ReinforcedBeam.Calculator();
				case BeamType.PrestressedBeam: return new Services.Calculators.PrestressedBeam.Calculator();
				default: return null;
			}
		}

		private static IBeamDataMapper GetMapper(BeamType type)
		{
			switch (type)
			{
				case BeamType.DeepBeam: return new DeepBeamMapper();
				case BeamType.FixedBeam: return new FixedBeamMapper { CrackViewsCount = 6, IntactViewsCount = 4 };
				case BeamType.ReinforcedBeam: return new ReinforcedBeamMapper { CrackViewsCount = 6, IntactViewsCount = 4 };
				case BeamType.PrestressedBeam:
					return new PrestressedBeamMapper
					{
						CrackViewsCount = 5,
						DeflectedUpViewsCount = 5,
						DeflectedDownViewsCount = 5
					};

				default: return null;
			}
		}
	}
}