﻿using Models;

namespace BeamCalculator.Infrastructure
{
	/// <summary>The factory that creates beam calculation service according to the beam type.</summary>
	public interface ICalculationServiceFactory
	{
		ICalculationService GetCalculationService(BeamType type);
	}
}
