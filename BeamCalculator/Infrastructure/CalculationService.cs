﻿using Models;
using Helpers.Math;
using Services.Calculators;
using BeamCalculator.ViewModels;
using System.Collections.Generic;
using BeamCalculator.Infrastructure.BeamCalcDataMappers;

namespace BeamCalculator.Infrastructure
{
	public class CalculationService : ICalculationService
	{
		private readonly IBeamCalculator _Calculator;
		private readonly IBeamDataMapper _Mapper;

		public CalculationService(IBeamCalculator calculator, IBeamDataMapper mapper)
		{
			_Calculator = calculator;
			_Mapper = mapper;
		}

		public Dictionary<int, BeamCalcDataViewModel> Calculate(BeamInputParameters parameters)
		{
			var ps = parameters.Clone();
			PrepareBeamParameters(ps);

			var data = _Calculator.Calculate(ps);
			return _Mapper.Map(data, ps.UnitsSystem);
		}

		private static void PrepareBeamParameters(BeamInputParameters ps)
		{
			switch (ps.UnitsSystem)
			{
				case MeasureUnitsSystem.Tech:
				case MeasureUnitsSystem.None: break;

				case MeasureUnitsSystem.Si:
					ps.ConcretePrismStrength = Algorithms.ConvertMPaToPa(ps.ConcretePrismStrength);
					ps.ConcreteTensionStrength = Algorithms.ConvertMPaToPa(ps.ConcreteTensionStrength);
					ps.ConcreteElasticityModulus = Algorithms.ConvertMPaToPa(ps.ConcreteElasticityModulus);

					ps.ReinforcementPrestress = Algorithms.ConvertMPaToPa(ps.ReinforcementPrestress);
					ps.ReinforcementTensionStrength = Algorithms.ConvertMPaToPa(ps.ReinforcementTensionStrength);
					ps.ReinforcementElasticityModulus = Algorithms.ConvertMPaToPa(ps.ReinforcementElasticityModulus);
					break;
			}

			switch (ps.BeamType)
			{
				case BeamType.FixedBeam:
					ps.CalculationLoadStep = ps.UnitsSystem == MeasureUnitsSystem.Si ? 500 : 50;
					ps.MaxIterationsNumber = 10000;
					break;

				case BeamType.ReinforcedBeam:
				case BeamType.PrestressedBeam:
					ps.MaxIterationsNumber = 5000;
					ps.CalculationLoadStep = ps.OutputLoadStep;
					break;
			}
		}
	}
}