﻿using System.Collections.Generic;
using BeamCalculator.ViewModels;
using Models;

namespace BeamCalculator.Infrastructure
{
	public interface ICalculationService
	{
		Dictionary<int, BeamCalcDataViewModel> Calculate(BeamInputParameters ps);
	}
}
