﻿using DI;
using Services.Infrastructure;

namespace Services
{
	public class Bootstrapper : IRegistrationService
	{
		public IContainer RegisterDependencies(IContainer container)
		{
			container.RegisterSingleton<DataRepository>();

			return container;
		}
	}
}