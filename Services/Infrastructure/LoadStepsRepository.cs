﻿using System.Collections.Generic;

namespace Services.Infrastructure
{
	/// <summary>Data required to run application.</summary>
	public partial class DataRepository
	{
		/// <summary>Beam loading steps.</summary>
		public IEnumerable<double> SiLoadSteps => _SiLoadSteps;
		private static readonly IEnumerable<double> _SiLoadSteps = new[]{2500D, 5000D, 10000D, 25000D, 50000D, 100000D, 150000D};
	}
}