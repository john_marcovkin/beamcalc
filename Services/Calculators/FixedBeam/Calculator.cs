﻿using Models;
using Services.Calculators.Resources;
using System;
using System.Collections.Generic;

namespace Services.Calculators.FixedBeam
{
	public class Calculator : IBeamCalculator
	{
		public BeamType BeamType => BeamType.FixedBeam;

		public IEnumerable<BeamCalculatedValues> Calculate(BeamInputParameters ps)
		{
			if (ps.ConcreteElasticityModulus <= double.Epsilon)
				throw new ArgumentException(Messages.ErrorZeroValue, nameof(BeamInputParameters.ConcreteElasticityModulus));
			
			// Max deflection before cracks:
			var ε0 = ps.ConcreteTensionStrength / (2 * ps.ConcreteElasticityModulus); 
			var δ0 = GetDeflection(ε0, ps.Length);

			var σ = 0D; // compression
			var ξ = 0D; // crack height
			var load = ps.BeamWeight;

			// The limit of while cycle:
			var iteration = 0;
			var data = new List<BeamCalculatedValues>();

			while (σ < ps.ConcretePrismStrength && iteration++ < ps.MaxIterationsNumber)
			{
				σ = GetCompression(load, ξ, ps);
				var δ = GetDeflection(σ / ps.ConcreteElasticityModulus, ps.Length); // deflection
				ξ = σ <= ps.ConcreteTensionStrength ? 0 : δ - δ0; // crack height

				var l = load - ps.BeamWeight;
				if (Math.Truncate(l % ps.OutputLoadStep) > double.Epsilon)
				{
					load += ps.CalculationLoadStep;
					continue;
				}

				var d = new BeamCalculatedValues
				{
					BeamWeight = ps.BeamWeight,
					Load = load - ps.BeamWeight,
					Deflection = δ,
					CracksHeight = ξ,
					ConcreteCompression = σ,

					ConcreteTension = (σ > ps.ConcreteTensionStrength) 
						? ps.ConcreteTensionStrength
						: σ
				};
				
				data.Add(d);
				load += ps.CalculationLoadStep;
			}

			return data;
		}

		private static double GetDeflection(double ε, double l) => l * Math.Sqrt(ε * (ε + 2)) / 2;

		/// <summary></summary>
		/// <param name="load">Load.</param>
		/// <param name="ξ">Crack height.</param>
		/// <param name="ps"></param>
		private static double GetCompression(double load, double ξ, BeamInputParameters ps)
		{
			var f = ps.Width * (ps.Height - ξ);
			var h = ps.Height - ξ;

			return load * ps.Length / (4 * f * h);
		}
	}
}