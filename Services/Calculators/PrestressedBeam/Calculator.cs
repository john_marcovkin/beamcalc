﻿using Models;

namespace Services.Calculators.PrestressedBeam
{
	public class Calculator : ReinforcedBeam.Calculator
	{
		public override BeamType BeamType => BeamType.PrestressedBeam;
	}
}