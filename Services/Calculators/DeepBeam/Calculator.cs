﻿using System.Collections.Generic;
using Models;

namespace Services.Calculators.DeepBeam
{
	public class Calculator : IBeamCalculator
	{
		public BeamType BeamType => BeamType.DeepBeam;

		public int IterationCount { get; set; } = 50000;
		public IEnumerable<BeamCalculatedValues> Calculate(BeamInputParameters parameters)
		{
			throw new System.NotImplementedException();
		}
	}
}