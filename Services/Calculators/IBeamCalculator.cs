﻿using System.Collections.Generic;
using Models;

namespace Services.Calculators
{
	public interface IBeamCalculator
	{
		BeamType BeamType { get; }
		IEnumerable<BeamCalculatedValues> Calculate(BeamInputParameters parameters);
	}
}