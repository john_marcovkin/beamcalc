﻿using System;
using Models;
using Helpers.Math;
using System.Collections.Generic;

namespace Services.Calculators.ReinforcedBeam
{
	public class Calculator : IBeamCalculator
	{
		public virtual BeamType BeamType => BeamType.ReinforcedBeam;

		public virtual IEnumerable<BeamCalculatedValues> Calculate(BeamInputParameters parameters)
		{
			var iteration = 0;
			var ps = parameters.Clone();

			var load = ps.BeamWeight;
			var data = new List<BeamCalculatedValues>();

			var σrPs = GetPrestressAfterRelaxation(ps);
			var σcPs = σrPs * ps.ReinforcementSectionArea / GetBeamCrossSectionArea(ps, 0);
			ps.ConcreteTensionStrength += σcPs;
			ps.ReinforcementTensionStrength -= σrPs;

			// Always add the precise values of the beam state at the 1st crack phase:
			var firstCrackAdded = false;
			var firstCrackValues = CalculateBeam(ps, double.NaN, 0);
			if (ps.ReinforcementPrestress > 0) CorrectPrestressOutput(firstCrackValues, ps, σcPs, σrPs);

			// Add failure value at the end of output:
			var failureValues = GetFailureValues(ps);
			if (ps.ReinforcementPrestress > 0) CorrectPrestressOutput(failureValues, ps, σcPs, σrPs);

			while (iteration++ < ps.MaxIterationsNumber)
			{
				// Add precise first crack values:
				if (!firstCrackAdded && load >= firstCrackValues.Load + ps.BeamWeight)
				{
					data.Add(firstCrackValues);
					firstCrackAdded = true;
				}

				var ξ = load <= firstCrackValues.Load + ps.BeamWeight ? 0 : GetCracksHeight(load, ps);
				var d = CalculateBeam(ps, load, ξ);
				if (ps.ReinforcementPrestress > 0) CorrectPrestressOutput(d, ps, σcPs, σrPs);

				// Add precise failure values and abort the cycle:
				if (d.ConcreteCompression >= ps.ConcretePrismStrength ||
					d.ReinforcementTension - σrPs >= ps.ReinforcementTensionStrength)
				{
					data.Add(failureValues);
					break;
				}

				data.Add(d);
				load += ps.CalculationLoadStep;
			}

			return data;
		}

		/// <summary>Calculates beam's deflection.</summary>
		/// <param name="ε">Beam relative deformation along its length direction.</param>
		/// <param name="l">Initial length of the beam.</param>
		protected static double GetDeflection(double ε, double l) => Math.Sqrt(Math.Abs(ε*(ε + 2))) *l/2;

		protected static double GetBeamCrossSectionArea(BeamInputParameters ps, double crackHeight)
		{
			if (crackHeight <= double.Epsilon) return
					ps.Width0 * ps.Height0 +
					ps.Width1 * ps.Height1 +
					ps.Width2 * ps.Height2;

			if (crackHeight < ps.Height2) return
					ps.Width0 * ps.Height0 +
					ps.Width1 * ps.Height1 +
					ps.Width2 * (ps.Height2 - crackHeight);

			var h = ps.Height2 + ps.Height1;
			if (crackHeight < h) return
					ps.Width0 * ps.Height0 +
					ps.Width1 * (h - crackHeight);

			h = ps.Height2 + ps.Height1 + ps.Height0;
			if (crackHeight < h) return
					ps.Width0 * (h - crackHeight);

			return 0;
		}



		private static double Sq(double d) => Math.Pow(d, 2);

		/// <summary>Calculates beam state based on its parameters, external load and crack height.</summary>
		/// <param name="ps">Beam parameters.</param>
		/// 
		/// <param name="load">
		/// External load. Set it to Double.NoN  and also crack between 
		/// 0 and beam height to calculate the load value automatically
		/// (crack = 0 - first crack load, crack > 0 - the load value
		/// relevant to this crack value). Otherwise load >= 0.
		/// </param>
		/// 
		/// <param name="crack">Crack height: between 0 and  beam height.</param>
		private static BeamCalculatedValues CalculateBeam(BeamInputParameters ps, double load, double crack)
		{
			var ξ = crack;
			var L = ps.Length;
			var H = ps.Height0 + ps.Height1 + ps.Height2;
			var Lf = L - 2*H*ps.Friction; // minus the moment of friction forces.

			var μ = ps.ConcretePoissonRate;
			var Ec = ps.ConcreteElasticityModulus;

			var Fa = ps.ReinforcementSectionArea;
			var ha = ps.ReinforcementLocationHeight;
			var Ea = ps.ReinforcementElasticityModulus;

			var hz = (H - ξ) * μ; // h*
			var Fc = GetBeamCrossSectionArea(ps, ξ);
			var Ka = Ea * Fa * ha / (Ec * Fc * hz);


			var σct = double.IsNaN(load) ? ps.ConcreteTensionStrength : load*Lf / (4*Fc*(hz + Ka*ha));
			var P = double.IsNaN(load) ? 4*σct*Fc*(hz + Ka*ha) / Lf : load * Lf/L; // load value;
			var σa = P * Lf / (4 * Fa * (hz / Ka + ha)); // reinforcement tension stress.
			var σp = σa * Fa / Fc; // concrete compression stress.
			var ε = σa / (16 * Ea); // relative deformation.
			var δ = GetDeflection(ε, L); // deflection.

			return new BeamCalculatedValues
			{
				Deflection = δ,
				CracksHeight = ξ,
				ConcreteTension = σct,
				ConcreteCompression = σp,
				ReinforcementTension = σa,

				Load = double.IsNaN(load) ? P * L / Lf - ps.BeamWeight : load - ps.BeamWeight,
				BeamWeight = ps.BeamWeight
			};
		}

		/// <summary>
		/// First, calculates crack at the failure phase, and then using this crack value
		/// calculates other beam parameters: load, concrete and reinforcement stresses.
		/// </summary>
		private static BeamCalculatedValues GetFailureValues(BeamInputParameters ps)
		{
			// Calculates crack height at
			var h1 = ps.Height1 + ps.Height2;
			var h0 = ps.Height0 + ps.Height1 + ps.Height2;
			var c = GetCrackByReinforcementFailure(ps);

			// The crack reached the top of the beam:
			var c21 = GetCrackByConcreteFailure(ps, h0, ps.Width0, 0);
			var c22 = GetFailureCrackCubic(ps, h0, ps.Width0, 0);
			var min = Math.Min(c, Math.Min(c21, c22));
			if (min >= h1 && min <= h0) return CalculateBeam(ps, double.NaN, min);

			// The crack  reached the middle of the beam:
			var s = ps.Height0 * ps.Width0;
			c21 = GetCrackByConcreteFailure(ps, h1, ps.Width1, s);
			c22 = GetFailureCrackCubic(ps, h1, ps.Width1, s);
			min = Math.Min(c, Math.Min(c21, c22));
			if (min >= ps.Height2 && min <= h1) return CalculateBeam(ps, double.NaN, min);

			// The crack is at the bottom of the beam:
			s = ps.Height0 * ps.Width0 + ps.Height1 * ps.Width1;
			c21 = GetCrackByConcreteFailure(ps, ps.Height2, ps.Width2, s);
			c22 = GetFailureCrackCubic(ps, ps.Height2, ps.Width2, s);
			min = Math.Min(c, Math.Min(c21, c22));
			if (min >= 0 && min <= ps.Height2) return CalculateBeam(ps, double.NaN, min);

			return null;
		}

		/// <summary>
		/// Solves cubic equation based on the equation for the concrete tension 
		/// stress to find crack value and returns the minimum positive root. 
		/// There are 3 cases: 
		/// 1) The crack is at the bottom of the beam, 
		/// 2) The crack is at the middle of the beam, 
		/// 3) The crack is at the top of the beam.
		/// The solver starts from the 1st one and goes down to the last one each 
		/// time solving a new cubic equation until it found the correct value.
		/// </summary>
		private static double GetCracksHeight(double load, BeamInputParameters ps)
		{
			// The crack is within the  Height2 (bottom) area of the beam:
			var h = ps.Height2;
			var crack = GetCracksHeight(ps, load, ps.Width2, h,
						ps.Height0 * ps.Width0 + ps.Height1 * ps.Width1);

			if (!double.IsNaN(crack) && !double.IsInfinity(crack) && crack <= h) return crack;

			// The crack is within the middle area of the beam (Height2 area):
			h = ps.Height1 + ps.Height2;
			crack = GetCracksHeight(ps, load, ps.Width1, h, ps.Height0 * ps.Width0);
			if (!double.IsNaN(crack) && !double.IsInfinity(crack) && crack <= h) return crack;

			// The crack is at the top of the beam or higher ("strange" roots of the cubic equation):
			h = ps.Height0 + ps.Height1 + ps.Height2;
			crack = GetCracksHeight(ps, load, ps.Width0, h, 0);
			return (!double.IsNaN(crack) && !double.IsInfinity(crack) && crack <= h)
				? crack
				: h;
		}

		private static double GetCracksHeight(BeamInputParameters ps, double load, double w, double h, double s)
		{
			var H = ps.Height0 + ps.Height1 + ps.Height2;
			var A = load * ps.Length * ps.ConcretePoissonRate / 4;
			var B = ps.ReinforcementElasticityModulus * ps.ReinforcementSectionArea *
					Sq(ps.ReinforcementLocationHeight) / ps.ConcreteElasticityModulus;

			var σt = ps.ConcreteTensionStrength;
			var μ2σ = Sq(ps.ConcretePoissonRate) * σt;

			var a = -w * μ2σ;
			var b = μ2σ * (2 * H * w + h * w + s);
			var c = (-H * w - 2 * (h * w + s)) * H * μ2σ + A;
			var d = (h * w + s) * Sq(H) * μ2σ + B * σt - A * H;

			var crack = Algorithms.GetMinPositiveCubicRoot(a, b, c, d, H);
			return crack;
		}

		/// <summary>
		/// Solves linear equation to find the crack values at the
		/// failure phase  assuming that beam will fail because of
		/// the reinforcement stress exceeds tension strength value.
		/// </summary>
		private static double GetCrackByReinforcementFailure(BeamInputParameters ps)
		{
			var H = ps.Height0 + ps.Height1 + ps.Height2;
			var μ = ps.ConcretePoissonRate;
			var σt = ps.ConcreteTensionStrength;
			var Ec = ps.ConcreteElasticityModulus;

			var ha = ps.ReinforcementLocationHeight;
			var σa = ps.ReinforcementTensionStrength;
			var Ea = ps.ReinforcementElasticityModulus;

			var p0 = σa * Ec * μ;
			var p1 = p0 * H;
			var p2 = σt * Ea * ha;
			var crack = (p1 - p2) / p0;
			return crack >= 0 ? crack : H;
		}

		/// <summary>
		/// Solves linear equation to find the crack values at the
		/// failure phase  assuming that beam will fail because of
		/// the concrete compression stress exceeds prism strength
		/// value.
		/// </summary>
		private static double GetCrackByConcreteFailure(BeamInputParameters ps, double h, double w, double s)
		{
			var H = ps.Height0 + ps.Height1 + ps.Height2;
			var μ = ps.ConcretePoissonRate;
			var σp = ps.ConcretePrismStrength;
			var σt = ps.ConcreteTensionStrength;
			var Ec = ps.ConcreteElasticityModulus;
			var μEc = Ec * μ;

			var Fa = ps.ReinforcementSectionArea;
			var ha = ps.ReinforcementLocationHeight;
			var Ea = ps.ReinforcementElasticityModulus;
			var FaPs = Fa * GetPrestressAfterRelaxation(ps);

			var a = -μEc * σp * w;
			var b = μEc * (σp*((H + h) * w + s) - FaPs);
			var c = σt * Ea * Fa * ha - H * μEc * (σp*(h * w + s) - FaPs);
			return Algorithms.GetMinPositiveQuadraticRoot(a, b, c, H);
		}

		private static double GetPrestressAfterRelaxation(BeamInputParameters ps)
		{
			var εr = ps.ReinforcementPrestress / ps.ReinforcementElasticityModulus;
			var εc = ps.ReinforcementPrestress * ps.ReinforcementSectionArea /
			         (GetBeamCrossSectionArea(ps, 0) * ps.ConcreteElasticityModulus);

			return (εr - εc) * ps.ReinforcementElasticityModulus;
		}

		private void CorrectPrestressOutput(BeamCalculatedValues v, BeamInputParameters ps, double σcPs, double σrPs)
		{
			v.ConcreteTension -= σcPs;
			v.ReinforcementTension += σrPs;

			if (v.CracksHeight <= double.Epsilon) v.Deflection = 
					Math.Sign(v.ConcreteTension) * 
					GetDeflection(v.ConcreteTension / 
					ps.ConcreteElasticityModulus, ps.Length);

			v.ConcreteCompression = 
				v.ReinforcementTension * 
				ps.ReinforcementSectionArea / 
				GetBeamCrossSectionArea(ps, v.CracksHeight);
		}

		/// <summary>
		/// Solves additional cubic equation to find possible crack height.
		/// </summary>
		private static double GetFailureCrackCubic(BeamInputParameters ps, double h, double w, double s)
		{
			var H = ps.Height0 + ps.Height1 + ps.Height2;

			var Ec = ps.ConcreteElasticityModulus;
			var μ2 = Sq(ps.ConcretePoissonRate);
			var Ecμ2 = Ec * μ2;

			var Fa = ps.ReinforcementSectionArea;
			var ha = ps.ReinforcementLocationHeight;
			var Ea = ps.ReinforcementElasticityModulus;

			var a = Ecμ2 * w;
			var b = -Ecμ2 * (2 * H * w + w * h + s);
			var c = Ecμ2 * H * ((H + 2 * h) * w + 2 * s);
			var d = -Ecμ2 * Sq(H) * (w * h + s) - Ea * Fa * Sq(ha);
			return Algorithms.GetMinPositiveCubicRoot(a, b, c, d, H);
		}
	}
}