﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using Algorithms = Helpers.Math.Algorithms;

namespace WpfControls.ZoomPanScrollControl
{
	public class ZoomPanScroll: ScrollViewer
	{
		static ZoomPanScroll()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(ZoomPanScroll), new FrameworkPropertyMetadata(typeof(ZoomPanScroll)));

			ZoomStepProperty = DependencyProperty.Register(nameof(ZoomStep), typeof(double), typeof(ZoomPanScroll), new FrameworkPropertyMetadata(0.2D));
			MinScaleProperty = DependencyProperty.Register(nameof(MinScale), typeof(double), typeof(ZoomPanScroll), new FrameworkPropertyMetadata(0.1D));
			MaxScaleProperty = DependencyProperty.Register(nameof(MaxScale), typeof(double), typeof(ZoomPanScroll), new FrameworkPropertyMetadata(10D));
			ScaleProperty = DependencyProperty.Register(nameof(Scale), typeof(double), typeof(ZoomPanScroll), new FrameworkPropertyMetadata(1D));
			AlwaysFitToContentProperty = DependencyProperty.Register(nameof(AlwaysFitToContent), typeof(bool), typeof(ZoomPanScroll));
			FitWhenContentChangedProperty = DependencyProperty.Register(nameof(FitWhenContentChanged), typeof(bool), typeof(ZoomPanScroll));

			HorizontalScrollBarVisibilityProperty.OverrideMetadata(typeof(ZoomPanScroll), new FrameworkPropertyMetadata(ScrollBarVisibility.Auto));
			VerticalScrollBarVisibilityProperty.OverrideMetadata(typeof(ZoomPanScroll), new FrameworkPropertyMetadata(ScrollBarVisibility.Auto));
		}

		public static readonly DependencyProperty ZoomStepProperty;
		public static readonly DependencyProperty MinScaleProperty;
		public static readonly DependencyProperty MaxScaleProperty;
		public static readonly DependencyProperty ScaleProperty;
		public static readonly DependencyProperty AlwaysFitToContentProperty;
		public static readonly DependencyProperty FitWhenContentChangedProperty;
		
		public double ZoomStep
		{
			get => (double)GetValue(ZoomStepProperty);
			set => SetValue(ZoomStepProperty, value);
		}

		/// <summary>Gets or sets the minimum scale value.</summary>
		public double MinScale
		{
			get => (double)GetValue(MinScaleProperty);
			set => SetValue(MinScaleProperty, value);
		}

		/// <summary>Gets or sets the maximum scale value.</summary>
		public double MaxScale
		{
			get => (double)GetValue(MaxScaleProperty);
			set => SetValue(MaxScaleProperty, value);
		}

		/// <summary>Gets or sets the current scale value.</summary>
		public double Scale
		{
			get => (double)GetValue(ScaleProperty);
			set => SetValue(ScaleProperty, value);
		}

		/// <summary>Automatically adjust the content size to fit the ScrollViewer' area.</summary>
		public bool AlwaysFitToContent
		{
			get => (bool)GetValue(AlwaysFitToContentProperty);
			set => SetValue(AlwaysFitToContentProperty, value);
		}

		/// <summary>Automatically adjust the content size to fit the ScrollViewer' area each time when content is changed.</summary>
		public bool FitWhenContentChanged
		{
			get => (bool)GetValue(FitWhenContentChangedProperty);
			set => SetValue(FitWhenContentChangedProperty, value);
		}
		
		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
			_Content = (FrameworkElement)GetTemplateChild("Content");
			_ScrollContentPresenter = (FrameworkElement)GetTemplateChild("PART_ScrollContentPresenter");
		}

		public void FitToContent()
		{
			if(!(Content is FrameworkElement content)) return;
			var scrollSize = new Size(ViewportWidth, ViewportHeight);

			var m = content.Margin;
			var mw = (double.IsNaN(m.Left) ? 0 : m.Left) +
			         (double.IsNaN(m.Right) ? 0 : m.Right);
			var mh = (double.IsNaN(m.Top) ? 0 : m.Top) +
			         (double.IsNaN(m.Bottom) ? 0 : m.Bottom);
			var contentSize = new Size(content.ActualWidth + mw, content.ActualHeight + mh);

			var valid = !double.IsNaN(contentSize.Width) && contentSize.Width > 0 && 
			            !double.IsNaN(contentSize.Height) && contentSize.Height > 0 && 
			            !double.IsNaN(scrollSize.Width) && scrollSize.Width > 0 &&
			            !double.IsNaN(scrollSize.Height) && scrollSize.Height > 0;

			if (!valid) return;
			var size = Algorithms.GetInscribeRectangle(scrollSize, contentSize);
			Scale = size.Width == this.ViewportWidth
				? size.Width / contentSize.Width
				: size.Height / contentSize.Height;
		}

		private Point? _LastCenterPositionOnTarget;
		private Point? _LastMousePositionOnTarget;
		private Point? _LastDragPoint;


		private FrameworkElement _Content;
		private FrameworkElement _ScrollContentPresenter;
		private Size _InitialContentSize = new Size(0, 0);

		protected override void OnContentChanged(object oldContent, object newContent)
		{
			base.OnContentChanged(oldContent, newContent);
			if (!AlwaysFitToContent && FitWhenContentChanged) FitToContent();
		}

		protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			base.OnPreviewMouseLeftButtonDown(e);
			if (IsMouseOutsideOfScrollView) return;

			this.Cursor = Cursors.SizeAll;
			_LastDragPoint = e.GetPosition(this);
			this.CaptureMouse();
		}

		protected override void OnPreviewMouseLeftButtonUp(MouseButtonEventArgs e)
		{
			base.OnPreviewMouseLeftButtonUp(e);
			this.ReleaseMouseCapture();
			this.Cursor = null;
		}
		
		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove(e);

			// Handle only if the Mouse left button is pressed and Mouse 
			// pointer position is within the ScrollView's content area:
			if (IsMouseOutsideOfScrollView || 
				Mouse.LeftButton != MouseButtonState.Pressed || 
				!_LastDragPoint.HasValue) return;

			var current = e.GetPosition(this);
			var delta = current - _LastDragPoint;
			_LastDragPoint = current;

			ScrollToHorizontalOffset(HorizontalOffset - delta.Value.X);
			ScrollToVerticalOffset(VerticalOffset - delta.Value.Y);
		}

		protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
		{
			e.Handled = true;
			if(IsMouseOutsideOfScrollView) return;

			_LastMousePositionOnTarget = Mouse.GetPosition(_Content);

			var newScale = e.Delta > 0
				? Scale * (1 + ZoomStep)
				: Scale / (1 + ZoomStep);

			if (newScale < MinScale) newScale = MinScale;
			if (newScale > MaxScale) newScale = MaxScale;

			Scale = newScale;

			var centerOfViewport = new Point(this.ViewportWidth / 2, this.ViewportHeight / 2);
			_LastCenterPositionOnTarget = this.TranslatePoint(centerOfViewport, _Content);
		}

		protected override void OnScrollChanged(ScrollChangedEventArgs e)
		{
			if (e.ExtentHeightChange == 0 && e.ExtentWidthChange == 0) return;

			// Fit to content options:
			if (AlwaysFitToContent)
			{
				FitToContent();
				return;
			}

			// This detects if the content has been changed by examining its size:
			if (Scale > 0)
			{
				// Get initial content size before scaling and
				// compare it with the previous content size:
				var initialWidth = e.ExtentWidth / Scale;
				var initialHeight = e.ExtentHeight / Scale;
				var wDiff = Math.Round(initialWidth - _InitialContentSize.Width);
				var hDiff = Math.Round(initialHeight - _InitialContentSize.Height);

				if (wDiff != 0 || hDiff != 0)
				{
					_InitialContentSize = new Size(initialWidth, initialHeight);

					if (FitWhenContentChanged)
					{
						FitToContent();
						return;
					}
				}
			}

			// Scroll content when zooming:
			Point? targetBefore = null;
			Point? targetNow = null;

			if (!_LastMousePositionOnTarget.HasValue)
			{
				if (_LastCenterPositionOnTarget.HasValue)
				{
					var centerOfViewport = new Point(this.ViewportWidth / 2, this.ViewportHeight / 2);
					var centerOfTargetNow = this.TranslatePoint(centerOfViewport, _Content);

					targetBefore = _LastCenterPositionOnTarget;
					targetNow = centerOfTargetNow;
				}
			}
			else
			{
				targetBefore = _LastMousePositionOnTarget;
				targetNow = Mouse.GetPosition(_Content);

				_LastMousePositionOnTarget = null;
			}

			if (!targetBefore.HasValue) return;

			var dXInTargetPixels = targetNow.Value.X - targetBefore.Value.X;
			var dYInTargetPixels = targetNow.Value.Y - targetBefore.Value.Y;

			var scaleX = e.ExtentWidth / _Content.ActualWidth;
			var scaleY = e.ExtentHeight / _Content.ActualHeight;

			var newOffsetX = this.HorizontalOffset - dXInTargetPixels * scaleX;
			var newOffsetY = this.VerticalOffset - dYInTargetPixels * scaleY;

			if (double.IsNaN(newOffsetX) || double.IsNaN(newOffsetY)) return;

			this.ScrollToHorizontalOffset(newOffsetX);
			this.ScrollToVerticalOffset(newOffsetY);
		}

		private bool IsMouseOutsideOfScrollView
		{
			get
			{
				var position = Mouse.GetPosition(_ScrollContentPresenter);
				return position.X >= ViewportWidth || position.Y >= ViewportHeight;
			}
		}
	}
}