﻿using System.Windows;
using System.Windows.Controls;

namespace WpfControls.EquationControl
{
	public class EquationInfo : Freezable
	{
		public static readonly DependencyProperty EquationProperty;
		public static readonly DependencyProperty ValueOffsetProperty;
		public static readonly DependencyProperty EquationSizeProperty;
		public static readonly DependencyProperty EquationTooltipProperty;

		static EquationInfo()
		{
			EquationProperty = DependencyProperty.Register(nameof(Equation), typeof(object), typeof(EquationInfo));
			EquationSizeProperty = DependencyProperty.Register(nameof(EquationSize), typeof(double), typeof(EquationInfo));
			EquationTooltipProperty = DependencyProperty.Register(nameof(EquationTooltip), typeof(ToolTip), typeof(EquationInfo));
			ValueOffsetProperty = DependencyProperty.Register(nameof(ValueOffset), typeof(double), typeof(EquationInfo), 
				new FrameworkPropertyMetadata(0D));
		}

		public object Equation
		{
			get => GetValue(EquationProperty);
			set => SetValue(EquationProperty, value);
		}

		public double EquationSize
		{
			get => (double)GetValue(EquationSizeProperty);
			set => SetValue(EquationSizeProperty, value);
		}

		public ToolTip EquationTooltip
		{
			get => (ToolTip)GetValue(EquationTooltipProperty);
			set => SetValue(EquationTooltipProperty, value);
		}

		public double ValueOffset
		{
			get => (double)GetValue(ValueOffsetProperty);
			set => SetValue(ValueOffsetProperty, value);
		}

		protected override Freezable CreateInstanceCore() => new EquationInfo();
	}
}