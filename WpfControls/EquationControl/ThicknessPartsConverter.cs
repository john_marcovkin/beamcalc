﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Globalization;

namespace WpfControls.EquationControl
{
	public class ThicknessPartsConverter : IValueConverter
	{
		public bool SelectTop { get; set; }
		public bool SelectLeft { get; set; }
		public bool SelectRight { get; set; }
		public bool SelectBottom { get; set; }

		public bool SwapTopAndBottom { get; set; }
		public bool SwapLeftAndRight { get; set; }

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if(!(value is Thickness t) || targetType != typeof(Thickness))
				return new Thickness(0);
			
			if (SwapTopAndBottom)
			{
				var bottom = t.Bottom;
				t.Bottom = t.Top;
				t.Top = bottom;
			}

			if (SwapLeftAndRight)
			{
				var right = t.Right;
				t.Right = t.Left;
				t.Left = right;
			}

			var output = new Thickness
			{
				Top = SelectTop ? t.Top : 0,
				Left = SelectLeft ? t.Left : 0,
				Right = SelectRight ? t.Right : 0,
				Bottom = SelectBottom ? t.Bottom : 0
			};

			return output;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}