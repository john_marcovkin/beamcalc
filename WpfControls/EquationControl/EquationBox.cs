﻿using System;
using System.Globalization;
using System.Windows;
using WpfControls.Common;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Data;
using Xceed.Wpf.Toolkit;

namespace WpfControls.EquationControl
{
	public class EquationBox : Control
	{
		public static readonly DependencyProperty AngleProperty;
		public static readonly DependencyProperty ScaleProperty;
		public static readonly DependencyProperty EquationAlignmentProperty;
		public static readonly DependencyProperty UseRenderTransformProperty;

		public static readonly DependencyProperty EquationProperty;
		public static readonly DependencyProperty EquationSizeProperty;
		public static readonly DependencyProperty EquationSharedSizeGroupProperty;

		public static readonly DependencyProperty EquationValueProperty;
		public static readonly DependencyProperty EquationSmallNumberBoundProperty;
		public static readonly DependencyProperty EquationLargeNumberBoundProperty;
		public static readonly DependencyProperty EquationMinValueProperty;
		public static readonly DependencyProperty EquationMaxValueProperty;
		public static readonly DependencyProperty EquationValueTextFontProperty;
		public static readonly DependencyProperty EquationBaseNumberFormatStringProperty;
		public static readonly DependencyProperty EquationSmallNumberFormatStringProperty;
		public static readonly DependencyProperty EquationLargeNumberFormatStringProperty;
		public static readonly DependencyProperty EquationReadOnlyValueToStringConverterProperty;
		public static readonly DependencyProperty EquationValueTextProperty;
		public static readonly DependencyProperty EquationValueMarginProperty;

		public static readonly DependencyProperty EquationMeasureUnitProperty;
		public static readonly DependencyProperty EquationMeasureUnitSizeProperty;

		public static readonly DependencyProperty EquationWatermarkProperty;
		public static readonly DependencyProperty ReadOnlyProperty;

		static EquationBox()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(EquationBox), new FrameworkPropertyMetadata(typeof(EquationBox)));

			EquationProperty = DependencyProperty.Register(nameof(Equation), typeof(object), typeof(EquationBox));

			EquationValueProperty = DependencyProperty.Register(nameof(EquationValue), typeof(double?), typeof(EquationBox),
				new FrameworkPropertyMetadata(EquationValueChanged));

			EquationValueTextProperty = DependencyProperty.Register(nameof(EquationValueText), typeof(string), typeof(EquationBox));

			EquationValueTextFontProperty = DependencyProperty.Register(nameof(EquationValueTextFont), typeof(Font), typeof(EquationBox),
				new FrameworkPropertyMetadata(new Font { Size = 12D, Family = new FontFamily("Times New Roman")}));


			EquationSizeProperty = DependencyProperty.Register(nameof(EquationSize), typeof(double), typeof(EquationBox), 
				new FrameworkPropertyMetadata(double.NaN));

			AngleProperty = DependencyProperty.Register(nameof(Angle), typeof(double), typeof(EquationBox),
				new FrameworkPropertyMetadata(0D, TransformChanged));

			ScaleProperty = DependencyProperty.Register(nameof(Scale), typeof(double), typeof(EquationBox), new FrameworkPropertyMetadata(1D));

			UseRenderTransformProperty = DependencyProperty.Register(nameof(UseRenderTransform), typeof(bool), typeof(EquationBox),
				new FrameworkPropertyMetadata(true, TransformChanged));


			EquationAlignmentProperty = DependencyProperty.Register(nameof(EquationAlignment), typeof(HorizontalAlignment), typeof(EquationBox));

			EquationSharedSizeGroupProperty = DependencyProperty.Register(nameof(EquationSharedSizeGroup), typeof(string), typeof(EquationBox));


			EquationReadOnlyValueToStringConverterProperty = DependencyProperty.Register(nameof(EquationReadOnlyValueToStringConverter), typeof(IValueConverter), typeof(EquationBox),
				new FrameworkPropertyMetadata(EquationValueChanged));

			EquationBaseNumberFormatStringProperty = DependencyProperty.Register(nameof(EquationBaseNumberFormatString), typeof(string), typeof(EquationBox),
				new FrameworkPropertyMetadata("#,##0.#####", EquationValueChanged));

			EquationSmallNumberFormatStringProperty = DependencyProperty.Register(nameof(EquationSmallNumberFormatString), typeof(string), typeof(EquationBox),
				new FrameworkPropertyMetadata("E5", EquationValueChanged));

			EquationLargeNumberFormatStringProperty = DependencyProperty.Register(nameof(EquationLargeNumberFormatString), typeof(string), typeof(EquationBox),
				new FrameworkPropertyMetadata("E5", EquationValueChanged));

			EquationSmallNumberBoundProperty = DependencyProperty.Register(nameof(EquationSmallNumberBound), typeof(double), typeof(EquationBox),
				new FrameworkPropertyMetadata(0.001D, EquationValueChanged));

			EquationLargeNumberBoundProperty = DependencyProperty.Register(nameof(EquationLargeNumberBound), typeof(double), typeof(EquationBox),
				new FrameworkPropertyMetadata(1e7D, EquationValueChanged));


			EquationValueMarginProperty = DependencyProperty.Register(nameof(EquationValueMargin), typeof(Thickness), typeof(EquationBox),
				new FrameworkPropertyMetadata(EquationValueMarginChanged));

			EquationMaxValueProperty = DependencyProperty.Register(nameof(EquationMaxValue), typeof(double), typeof(EquationBox));
			EquationMinValueProperty = DependencyProperty.Register(nameof(EquationMinValue), typeof(double), typeof(EquationBox));

			EquationWatermarkProperty = DependencyProperty.Register(nameof(EquationWatermark), typeof(string), typeof(EquationBox));

			EquationMeasureUnitProperty = DependencyProperty.Register(nameof(EquationMeasureUnit), typeof(object), typeof(EquationBox));
			EquationMeasureUnitSizeProperty = DependencyProperty.Register(nameof(EquationMeasureUnitSize), typeof(double), typeof(EquationBox),
				new FrameworkPropertyMetadata(double.NaN));

			ReadOnlyProperty = DependencyProperty.Register(nameof(ReadOnly), typeof(bool), typeof(EquationBox));
		}

		public override void OnApplyTemplate()
		{
			_EquationPart = GetTemplateChild("PART_Equation");
			_EquationValuePart = GetTemplateChild("PART_EquationValue");
			_EquationMeasureUnitPart = GetTemplateChild("PART_EquationMeasureUnit");
			SetEquationMargin(this);

			base.OnApplyTemplate();
		}

		#region TRANSFORMS

		/// <summary>The angle of rotation of this Box. The default value = 0.</summary>
		public double Angle
		{
			get => (double)GetValue(AngleProperty);
			set => SetValue(AngleProperty, value);
		}

		/// <summary>The RenderTransform's scale of this Box. The default value = 1.</summary>
		public double Scale
		{
			get => (double)GetValue(ScaleProperty);
			set => SetValue(ScaleProperty, value);
		}

		public HorizontalAlignment EquationAlignment
		{
			get => (HorizontalAlignment)GetValue(EquationAlignmentProperty);
			set => SetValue(EquationAlignmentProperty, value);
		}

		/// <summary>
		/// When true, rotation and scale are applied to the RenderTransform property.
		/// Otherwise, they are applied to the LayoutTransform property.
		/// </summary>
		public bool UseRenderTransform
		{
			get => (bool)GetValue(UseRenderTransformProperty);
			set => SetValue(UseRenderTransformProperty, value);
		}

		#endregion TRANSFORMS

		#region EQUATION

		public object Equation
		{
			get => GetValue(EquationProperty);
			set => SetValue(EquationProperty, value);
		}
		public double EquationSize
		{
			get => (double )GetValue(EquationSizeProperty);
			set => SetValue(EquationSizeProperty, value);
		}

		public string EquationSharedSizeGroup
		{
			get => (string)GetValue(EquationSharedSizeGroupProperty);
			set => SetValue(EquationSharedSizeGroupProperty, value);
		}

		#endregion EQUATION

		#region EQUATION VALUE

		public double? EquationValue
		{
			get => (double?)GetValue(EquationValueProperty);
			set => SetValue(EquationValueProperty, value);
		}

		public Font EquationValueTextFont
		{
			get => (Font)GetValue(EquationValueTextFontProperty);
			set => SetValue(EquationValueTextFontProperty, value);
		}

		public string EquationBaseNumberFormatString
		{
			get => (string)GetValue(EquationBaseNumberFormatStringProperty);
			set => SetValue(EquationBaseNumberFormatStringProperty, value);
		}

		public double EquationSmallNumberBound
		{
			get => (double)GetValue(EquationSmallNumberBoundProperty);
			set => SetValue(EquationSmallNumberBoundProperty, value);
		}

		public double EquationLargeNumberBound
		{
			get => (double)GetValue(EquationLargeNumberBoundProperty);
			set => SetValue(EquationLargeNumberBoundProperty, value);
		}

		public string EquationSmallNumberFormatString
		{
			get => (string)GetValue(EquationSmallNumberFormatStringProperty);
			set => SetValue(EquationSmallNumberFormatStringProperty, value);
		}

		public string EquationLargeNumberFormatString
		{
			get => (string)GetValue(EquationLargeNumberFormatStringProperty);
			set => SetValue(EquationLargeNumberFormatStringProperty, value);
		}

		public IValueConverter EquationReadOnlyValueToStringConverter
		{
			get => (IValueConverter) GetValue(EquationReadOnlyValueToStringConverterProperty);
			set => SetValue(EquationReadOnlyValueToStringConverterProperty, value);
		}

		public string EquationValueText => 
			(string)GetValue(EquationValueTextProperty);

		public double EquationMaxValue
		{
			get => (double)GetValue(EquationMaxValueProperty);
			set => SetValue(EquationMaxValueProperty, value);
		}

		public double EquationMinValue
		{
			get => (double)GetValue(EquationMinValueProperty);
			set => SetValue(EquationMinValueProperty, value);
		}

		public string EquationWatermark
		{
			get => (string)GetValue(EquationWatermarkProperty);
			set => SetValue(EquationWatermarkProperty, value);
		}

		public Thickness EquationValueMargin
		{
			get => (Thickness)GetValue(EquationValueMarginProperty);
			set => SetValue(EquationValueMarginProperty, value);
		}

		#endregion EQUATION VALUE


		#region EQUATION MEASURE UNIT IMAGE

		public object EquationMeasureUnit
		{
			get => GetValue(EquationMeasureUnitProperty);
			set => SetValue(EquationMeasureUnitProperty, value);
		}

		public double  EquationMeasureUnitSize
		{
			get => (double)GetValue(EquationMeasureUnitSizeProperty);
			set => SetValue(EquationMeasureUnitSizeProperty, value);
		}

		#endregion EQUATION MEASURE UNIT IMAGE

		public bool ReadOnly
		{
			get => (bool)GetValue(ReadOnlyProperty);
			set => SetValue(ReadOnlyProperty, value);
		}

		private DependencyObject _EquationPart;
		private DependencyObject _EquationValuePart;
		private DependencyObject _EquationMeasureUnitPart;

		private static void TransformChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			d.ClearValue(RenderTransformProperty);
			d.ClearValue(LayoutTransformProperty);
			
			var angle = (double)d.GetValue(AngleProperty);
			if (double.IsNaN(angle)) angle = 0;
			if (e.Property == AngleProperty && e.NewValue is double a)
				angle = double.IsNaN(a) ? 0 : a;

			var t = new RotateTransform(angle);
			var useRenderTransform = (bool) d.GetValue(UseRenderTransformProperty);

			if (useRenderTransform) d.SetValue(RenderTransformProperty, t);
			else d.SetValue(LayoutTransformProperty, t);
		}

		private static void EquationValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if (!(d.GetValue(EquationValueProperty) is double value) || 
				double.IsNaN(value) || 
				double.IsInfinity(value))
			{
				d.SetValue(EquationValueTextProperty, string.Empty);
				return;
			}

			// For readonly settings:
			var converter = d.GetValue(EquationReadOnlyValueToStringConverterProperty) as IValueConverter;

			// For editable settings:
			string formatter;
			var abs = Math.Abs(value);
			var smallNumber = (double) d.GetValue(EquationSmallNumberBoundProperty);
			var largeNumber = (double) d.GetValue(EquationLargeNumberBoundProperty);

			if (abs <= smallNumber && abs > double.Epsilon)
				formatter = d.GetValue(EquationSmallNumberFormatStringProperty) as string ?? string.Empty;
			else if (abs >= largeNumber)
				formatter = d.GetValue(EquationLargeNumberFormatStringProperty) as string ?? string.Empty;
			else
				formatter = d.GetValue(EquationBaseNumberFormatStringProperty) as string ?? string.Empty;

			((DoubleUpDown)((EquationBox) d).GetTemplateChild("PART_EditableValue")).FormatString = formatter;

			if (converter == null)
				d.SetValue(EquationValueTextProperty, value.ToString(formatter, CultureInfo.CurrentUICulture));
			else
				d.SetValue(EquationValueTextProperty, converter.Convert(value, typeof(string), null, null));
		}

		private static void EquationValueMarginChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var eb = (EquationBox)d;
			if (e.NewValue != null && eb._EquationPart != null &&
			    eb._EquationValuePart != null & eb._EquationMeasureUnitPart != null)
			{
				SetEquationMargin(eb);
			}
		}

		private static void SetEquationMargin(DependencyObject d)
		{
			var eb = (EquationBox) d;
			var m = (Thickness)eb.GetValue(EquationValueMarginProperty);

			var em = new Thickness(0,0,m.Left,0);
			var um = new Thickness(m.Right,0,0,0);
			var vm = new Thickness(0, m.Top, 0, m.Bottom);

			eb._EquationPart.SetValue(MarginProperty, em);
			eb._EquationValuePart.SetValue(MarginProperty, vm);
			eb._EquationMeasureUnitPart.SetValue(MarginProperty, um);
		}
	}
}