﻿using System.Windows;

namespace WpfControls.EquationControl
{
	public class EquationMeasureUnitInfo : Freezable
	{
		public static readonly DependencyProperty ValueOffsetProperty;
		public static readonly DependencyProperty EquationMeasureUnitProperty;
		public static readonly DependencyProperty EquationMeasureUnitSizeProperty;

		static EquationMeasureUnitInfo()
		{
			ValueOffsetProperty = DependencyProperty.Register(nameof(ValueOffset), typeof(double), typeof(EquationMeasureUnitInfo));
			EquationMeasureUnitProperty = DependencyProperty.Register(nameof(EquationMeasureUnit), typeof(object), typeof(EquationMeasureUnitInfo));
			EquationMeasureUnitSizeProperty = DependencyProperty.Register(nameof(EquationMeasureUnitSize), typeof(double), typeof(EquationMeasureUnitInfo));
		}

		public double ValueOffset
		{
			get => (double)GetValue(ValueOffsetProperty);
			set => SetValue(ValueOffsetProperty, value);
		}

		public object EquationMeasureUnit
		{
			get => GetValue(EquationMeasureUnitProperty);
			set => SetValue(EquationMeasureUnitProperty, value);
		}

		public double EquationMeasureUnitSize
		{
			get => (double)GetValue(EquationMeasureUnitSizeProperty);
			set => SetValue(EquationMeasureUnitSizeProperty, value);
		}

		protected override Freezable CreateInstanceCore() => new EquationInfo();
	}
}