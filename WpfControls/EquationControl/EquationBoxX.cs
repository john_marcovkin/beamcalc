﻿using System;
using System.Globalization;
using Models;
using System.Windows;
using System.Windows.Controls;

namespace WpfControls.EquationControl
{
	public class EquationBoxX : EquationBox
	{
		public static readonly DependencyProperty EquationValueTypeProperty;
		public static readonly DependencyProperty EquationMeasureUnitsSystemProperty;
		public static readonly DependencyProperty EquationShowMinMaxInTooltipProperty;

		static EquationBoxX()
		{
			EquationValueTypeProperty = DependencyProperty.Register(nameof(EquationValueType), typeof(PhysicalValueType), typeof(EquationBoxX), 
				new FrameworkPropertyMetadata(PhysicalValueType.None, OnEquationChanged));

			EquationMeasureUnitsSystemProperty = DependencyProperty.Register(nameof(EquationMeasureUnitsSystem), typeof(MeasureUnitsSystem), typeof(EquationBoxX), 
				new FrameworkPropertyMetadata(MeasureUnitsSystem.None, OnEquationChanged));

			EquationShowMinMaxInTooltipProperty = DependencyProperty.Register(nameof(EquationShowMinMaxInTooltip), typeof(bool), typeof(EquationBoxX), 
				new FrameworkPropertyMetadata(true, OnEquationChanged));

			EquationMinValueProperty.OverrideMetadata(typeof(EquationBoxX), new FrameworkPropertyMetadata(OnEquationChanged));
			EquationMaxValueProperty.OverrideMetadata(typeof(EquationBoxX), new FrameworkPropertyMetadata(OnEquationChanged));
		}

		public PhysicalValueType EquationValueType
		{
			get => (PhysicalValueType) GetValue(EquationValueTypeProperty);
			set => SetValue(EquationValueTypeProperty, value);
		}

		public bool EquationShowMinMaxInTooltip
		{
			get => (bool) GetValue(EquationShowMinMaxInTooltipProperty);
			set => SetValue(EquationShowMinMaxInTooltipProperty, value);
		}

		public MeasureUnitsSystem EquationMeasureUnitsSystem
		{
			get => (MeasureUnitsSystem)GetValue(EquationMeasureUnitsSystemProperty);
			set => SetValue(EquationMeasureUnitsSystemProperty, value);
		}

		private static void OnEquationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var unitsSystem = (MeasureUnitsSystem)d.GetValue(EquationMeasureUnitsSystemProperty);
			var valueType = (PhysicalValueType)d.GetValue(EquationValueTypeProperty);

			// Set equation:
			if (valueType == PhysicalValueType.None)
			{
				SetEquation(d);
				return;
			}

			var equationInfo = (EquationInfo)Application.Current.FindResource($"EquationBoxSettings.{valueType.ToString()}");
			if (unitsSystem == MeasureUnitsSystem.None)
			{
				SetEquation(d, equationInfo);
				return;
			}

			// Set measure unit:
			var p = string.Empty;
			switch (valueType)
			{
				case PhysicalValueType.None:
				case PhysicalValueType.Friction:
				case PhysicalValueType.ConcretePoissonRate:
					SetEquation(d, equationInfo); return;

				case PhysicalValueType.ConcreteTension:
				case PhysicalValueType.ConcreteCompression:
				case PhysicalValueType.ConcretePrismStrength:
				case PhysicalValueType.ConcreteTensionStrength:
				case PhysicalValueType.ConcreteElasticityModulus:

				case PhysicalValueType.ReinforcementTension:
				case PhysicalValueType.ReinforcementPrestress:
				case PhysicalValueType.ReinforcementTensionStrength:
				case PhysicalValueType.ReinforcementElasticityModulus:
					p = "Stress"; break;

				case PhysicalValueType.Load:
				case PhysicalValueType.SoleWeight:
					p = "Load"; break;

				case PhysicalValueType.Crack:
				case PhysicalValueType.Width:
				case PhysicalValueType.Width0:
				case PhysicalValueType.Width1:
				case PhysicalValueType.Width2:
				case PhysicalValueType.Length:
				case PhysicalValueType.Height:
				case PhysicalValueType.Height0:
				case PhysicalValueType.Height1:
				case PhysicalValueType.Height2:
				case PhysicalValueType.Deflection:
				case PhysicalValueType.ReinforcementLocationHeight:
					p = "Length"; break;

				case PhysicalValueType.ReinforcementSectionArea:
					p = "Area"; break;
			}

			var unitInfo = (EquationMeasureUnitInfo)Application.Current.FindResource
				($"EquationBoxSettings.{unitsSystem.ToString()}.{p}");

			SetEquation(d, equationInfo, unitInfo);
		}

		private static void SetEquation(DependencyObject d, EquationInfo ei = null, EquationMeasureUnitInfo emi = null)
		{
			if (ei == null)
			{
				d.ClearValue(ToolTipProperty);
				d.ClearValue(EquationProperty);
				d.ClearValue(EquationSizeProperty);

				d.ClearValue(EquationValueMarginProperty);

				d.ClearValue(EquationMeasureUnitProperty);
				d.ClearValue(EquationMeasureUnitSizeProperty);
				return;
			}
			
			SetTooltip(d, ei);
			d.SetValue(EquationProperty, ei.Equation);
			d.SetValue(EquationSizeProperty, ei.EquationSize);
			d.SetValue(EquationValueMarginProperty, new Thickness(ei.ValueOffset, 0, 0, 0));

			if (emi == null)
			{
				d.ClearValue(EquationMeasureUnitProperty);
				d.ClearValue(EquationMeasureUnitSizeProperty);
				return;
			}

			d.SetValue(EquationMeasureUnitProperty, emi.EquationMeasureUnit);
			d.SetValue(EquationMeasureUnitSizeProperty, emi.EquationMeasureUnitSize);
			d.SetValue(EquationValueMarginProperty, new Thickness(ei.ValueOffset, 0, emi.ValueOffset, 0));
		}

		private static void SetTooltip(DependencyObject d, EquationInfo ei)
		{
			if (!(ei?.EquationTooltip.Content is string)) return;

			var showMaxMin = (bool)d.GetValue(EquationShowMinMaxInTooltipProperty);
			if (!showMaxMin)
			{
				d.SetValue(ToolTipProperty, ei.EquationTooltip);
				return;
			}

			// Max and min:
			var min = d.GetValue(EquationMinValueProperty);
			var max = d.GetValue(EquationMaxValueProperty);

			var validMin = IsValidDouble(min, out var minD);
			var validMax = IsValidDouble(max, out var maxD);

			d.ClearValue(ToolTipProperty);
			string minS = null;
			string maxS = null;

			if (validMax && validMin)
			{
				minS = $"min: {DoubleToString(minD)}";
				maxS = $"max: {DoubleToString(maxD)}";
			}
			else if (validMin)
			{
				minS = $"min: {DoubleToString(minD)}";
				maxS = $"max: {double.MaxValue}";
			}
			else if (validMax)
			{
				minS = $"min: {double.MinValue}";
				maxS = $"max: {DoubleToString(maxD)}";
			}

			// Tooltip:
			var text = $"{minS}{(maxS == null ? string.Empty : ", ")}{maxS}";
			var toolTip = string.IsNullOrWhiteSpace(text)
				? ei.EquationTooltip
				: new ToolTip { Content = $"{ei.EquationTooltip.Content}. {text}" };

			d.SetValue(ToolTipProperty, toolTip);
		}

		private static bool IsValidDouble(object o, out double result)
		{
			if (o is double d && !double.IsNaN(d) && !double.IsInfinity(d))
			{
				result = d;
				return true;
			}

			result = double.NaN;
			return false;
		}

		private static string DoubleToString(double d)
		{
			var abs = Math.Abs(d);
			return 10e-4 <= abs && abs <= 10000000D || abs <= double.Epsilon
				? d.ToString("#,##0.#####", CultureInfo.CurrentUICulture)
				: d.ToString("E4");
		}
	}
}