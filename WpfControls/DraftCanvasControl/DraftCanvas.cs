﻿using System.Linq;
using System.Windows;
using System.Collections;
using System.Windows.Media;
using System.Windows.Controls;
using System.Collections.Generic;

using ME = Helpers.Math.Algorithms;
using MA = Helpers.Math.Algorithms;

namespace WpfControls.DraftCanvasControl
{
    public class DraftCanvas : Canvas
    {
		static DraftCanvas()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(DraftCanvas), new FrameworkPropertyMetadata(typeof(DraftCanvas)));

			BackgroundProperty.OverrideMetadata(typeof(DraftCanvas), new FrameworkPropertyMetadata(BackgroundChanged));

			ItemTemplateProperty = DependencyProperty.Register(nameof(ItemTemplate), typeof(DataTemplate), typeof(DraftCanvas));
			ItemsSourceProperty = DependencyProperty.Register(nameof(ItemSource), typeof(IEnumerable), typeof(DraftCanvas),
				new FrameworkPropertyMetadata(ItemsSourceChanged));
		}

	    public static readonly DependencyProperty ItemsSourceProperty;
	    public static readonly DependencyProperty ItemTemplateProperty;

		/// <summary>The template of the items added to Canvas.</summary>
		public DataTemplate ItemTemplate
	    {
		    get => (DataTemplate) GetValue(ItemTemplateProperty);
		    set => SetValue(ItemTemplateProperty, value);
	    }

		/// <summary>The items added to Canvas.</summary>
	    public IEnumerable ItemSource
	    {
		    get => (IEnumerable)GetValue(ItemsSourceProperty);
		    set => SetValue(ItemsSourceProperty, value);
		}

		private static void BackgroundChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
	    {
		    var canvas = (Canvas) d;

		    if (!(e.NewValue is ImageBrush ib)) return;

		    canvas.Width = ib.Viewbox.Width;
		    canvas.Height = ib.Viewbox.Height;
	    }

	    private static void ItemsSourceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
	    {
			if(sender is DraftCanvas dc) dc.OnItemsSourceChanged(e);
	    }


		protected override Size ArrangeOverride(Size arrangeSize)
	    {
		    var output = base.ArrangeOverride(arrangeSize);

			
			var margins = new List<Thickness>();
		    foreach (var e in InternalChildren)
				margins.Add(GetElementOverflow((UIElement)e));

			if(margins.Any())
				Margin = new Thickness
				{
					Top = ME.Max(margins.Select(m => m.Left)),
					Left = ME.Max(margins.Select(m => m.Left)),
					Right = ME.Max(margins.Select(m => m.Right)),
					Bottom = ME.Max(margins.Select(m => m.Bottom))
				};

		    return output;
	    }

		protected virtual void OnItemsSourceChanged(DependencyPropertyChangedEventArgs e)
	    {
		    if(e.OldValue != null) InternalChildren.Clear();

			if(e.NewValue is IEnumerable items && ItemTemplate != null)
				foreach (var item in items)
					if (ItemTemplate.LoadContent() is FrameworkElement control)
					{
						control.DataContext = item;
						InternalChildren.Add(control);
					}
	    }

		private Thickness GetElementOverflow(UIElement e)
	    {
			
		    var transform = GetElementTransform(e);
			var point = new Point(GetTop(e), GetLeft(e));
			var cSize = new Size(ActualWidth, ActualHeight);
			var eSize = new Size(e.RenderSize.Width * transform.ScaleX, e.RenderSize.Height * transform.ScaleY);

			return MA.GetOverflowValue(point, cSize, eSize, transform.Angle);
	    }

	    private static TransformInfo GetElementTransform(UIElement e)
	    {
			var scales = new List<ScaleTransform>();
			var rotates = new List<RotateTransform>();

		    if (e.RenderTransform is TransformGroup tg)
		    {
			    foreach (var t in tg.Children)
			    {
				    if(t is ScaleTransform st) scales.Add(st);
					if(t is RotateTransform rt) rotates.Add(rt);
			    }
		    }

			if (e.RenderTransform is ScaleTransform sct) scales.Add(sct);
		    if (e.RenderTransform is RotateTransform rtt) rotates.Add(rtt);

			var output = new TransformInfo();

		    foreach (var t in scales)
		    {
			    output.ScaleX = double.IsNaN(output.ScaleX) 
					? t.ScaleX 
					: output.ScaleX + t.ScaleX;

			    output.ScaleY = double.IsNaN(output.ScaleY)
				    ? t.ScaleY
				    : output.ScaleY + t.ScaleY;
			}

		    foreach (var t in rotates) output.Angle += t.Angle;
			if (double.IsNaN(output.ScaleX)) output.ScaleX = 1;
		    if (double.IsNaN(output.ScaleY)) output.ScaleY = 1;

		    return output;
	    }
		
		private class TransformInfo
		{
			internal double Angle { get; set; }
			internal double ScaleX { get; set; } = double.NaN;
			internal double ScaleY { get; set; } = double.NaN;
		}
    }
}