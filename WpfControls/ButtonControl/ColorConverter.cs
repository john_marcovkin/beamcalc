﻿using System;
using Helpers.Math;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Media;

namespace WpfControls.ButtonControl
{
	public class ColorConverter : IValueConverter
	{
		/// <summary>
		/// 0 - do not change the tint.<para/>
		/// TintRange between 0 and -1 (including -1 only): move hue to the yellow-red zone by specified rate (percent/100).<para/>
		/// TintRange between 0 and 1 (including 1 only): move hue to the blue-red zone by specified rate (percent/100).<para/>
		/// </summary>
		public double HueRate { get; set; } = 0;

		/// <summary>
		/// 0 - do not change the brightness.<para/>
		/// SaturationRate between 0 and -1 (including -1 only): fade the tint by specified rate (percent/100). -1 = white.<para/>
		/// SaturationRate between 0 and 1 (including 1 only): saturate the tint by specified rate (percent/100).<para/>
		/// </summary>
		public double SaturationRate { get; set; } = 0;

		/// <summary>
		/// 0 - do not change the saturation.<para/>
		/// sRate between 0 and -1 (including -1 only): make the tint darker by specified rate (percent/100). -1 = black.<para/>
		/// sRate between 0 and 1 (including 1 only): make the tint brighter by specified rate (percent/100).<para/>
		/// </summary>
		public double BrightnessRate { get; set; } = 0;

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is SolidColorBrush sb) || targetType != typeof(Brush)) return default(SolidColorBrush);
			var color = Algorithms.ChangeColor(sb.Color, HueRate, SaturationRate, BrightnessRate);
			return new SolidColorBrush(color);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}