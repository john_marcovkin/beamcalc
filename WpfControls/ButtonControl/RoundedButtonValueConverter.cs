﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Globalization;

namespace WpfControls.ButtonControl
{
	public class RoundedButtonValueConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var r = (double)value / 2;
			return new CornerRadius(r);
		}
			

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}