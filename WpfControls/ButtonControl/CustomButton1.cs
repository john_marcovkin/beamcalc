﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;

namespace WpfControls.ButtonControl
{
	public class CustomButton1 : Button
	{
		static CustomButton1()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(CustomButton1), new FrameworkPropertyMetadata(typeof(CustomButton1)));

			BaseStaticColorProperty = DependencyProperty.Register(nameof(BaseStaticColor), typeof(Brush), typeof(CustomButton1));
			BaseActionColorProperty = DependencyProperty.Register(nameof(BaseActionColor), typeof(Brush), typeof(CustomButton1));
			BaseDisabledColorProperty = DependencyProperty.Register(nameof(BaseDisabledColor), typeof(Brush), typeof(CustomButton1));

			CornerRadiusProperty = DependencyProperty.Register(nameof(CornerRadius), typeof(CornerRadius), typeof(CustomButton1));
		}

		public static readonly DependencyProperty BaseStaticColorProperty;
		public static readonly DependencyProperty BaseActionColorProperty;
		public static readonly DependencyProperty BaseDisabledColorProperty;
		public static readonly DependencyProperty CornerRadiusProperty;
		
		public CornerRadius CornerRadius
		{
			get => (CornerRadius)GetValue(CornerRadiusProperty);
			set => SetValue(CornerRadiusProperty, value);
		}

		public Brush BaseStaticColor
		{
			get => (Brush)GetValue(BaseStaticColorProperty);
			set => SetValue(BaseStaticColorProperty, value);
		}

		public Brush BaseActionColor
		{
			get => (Brush)GetValue(BaseActionColorProperty);
			set => SetValue(BaseActionColorProperty, value);
		}

		public Brush BaseDisabledColor
		{
			get => (Brush)GetValue(BaseDisabledColorProperty);
			set => SetValue(BaseDisabledColorProperty, value);
		}
	}
}