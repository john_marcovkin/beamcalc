﻿using System.Windows;
using System.Windows.Media;

namespace WpfControls.Common
{
    public class Font : Freezable
    {
	    protected override Freezable CreateInstanceCore() => this;

	    static Font()
	    {
		    FamilyProperty = DependencyProperty.Register(nameof(Family), typeof(FontFamily), typeof(Font));
		    WeightProperty = DependencyProperty.Register(nameof(Weight), typeof(FontWeight), typeof(Font));
		    StyleProperty = DependencyProperty.Register(nameof(Style), typeof(FontStyle), typeof(Font));
		    SizeProperty = DependencyProperty.Register(nameof(Size), typeof(double), typeof(Font));
		}

	    public static readonly DependencyProperty FamilyProperty;
	    public static readonly DependencyProperty WeightProperty;
	    public static readonly DependencyProperty StyleProperty;
	    public static readonly DependencyProperty SizeProperty;

	    public FontFamily Family
	    {
		    get => (FontFamily)GetValue(FamilyProperty);
		    set => SetValue(FamilyProperty, value);
		}

	    public FontWeight Weight
		{
		    get => (FontWeight)GetValue(WeightProperty);
		    set => SetValue(WeightProperty, value);
		}

	    public FontStyle Style
	    {
		    get => (FontStyle)GetValue(StyleProperty);
		    set => SetValue(StyleProperty, value);
		}

	    public double Size
		{
		    get => (double)GetValue(SizeProperty);
		    set => SetValue(SizeProperty, value);
	    }
	}
}