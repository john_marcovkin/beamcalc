﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Helpers
{
	public static class AssemblyResolver
	{
		public static Assembly GetReferencedAssembly(Assembly root, string referencedAssemblyName)
		{
			var rName = Assembly.GetEntryAssembly()
				.GetReferencedAssemblies()
				.FirstOrDefault(a => a.Name == referencedAssemblyName);

			if (rName == null)
				throw new FileNotFoundException($"Cannot load {referencedAssemblyName} assembly");

			var assembly = AppDomain.CurrentDomain
				.GetAssemblies()
				.FirstOrDefault(a => a.FullName == rName.FullName);

			return assembly ?? AppDomain.CurrentDomain.Load(rName);
		}
	}
}