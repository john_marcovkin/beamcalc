﻿using System.Windows.Media;
using ColorMine.ColorSpaces;
using M = System.Math;

namespace Helpers.Math
{
	public static partial class Algorithms
	{
		/// <summary>
		/// Changes the tint, saturation and brightness of the
		/// specified color according to HSB (HSV) color model.
		/// </summary>
		///
		/// <param name="c">The specified color.</param>
		/// 
		/// <param name="hRate">
		/// 0 - do not change the tint.<para/>
		/// hRate between 0 and -1 (including -1 only): move hue to the yellow-red zone by specified rate (percent/100).<para/>
		/// hRate between 0 and 1 (including 1 only): move hue to the blue-red zone by specified rate (percent/100).<para/>
		/// </param>
		/// 
		/// <param name="sRate">
		/// 0 - do not change the brightness.<para/>
		/// sRate between 0 and -1 (including -1 only): fade the tint by specified rate (percent/100). -1 = white.<para/>
		/// sRate between 0 and 1 (including 1 only): saturate the tint by specified rate (percent/100).<para/>
		/// </param>
		///
		/// <param name="bRate">
		/// 0 - do not change the saturation.<para/>
		/// bRate between 0 and -1 (including -1 only): make the tint darker by specified rate (percent/100). -1 = black.<para/>
		/// bRate between 0 and 1 (including 1 only): make the tint brighter by specified rate (percent/100).<para/>
		/// </param>
		/// 
		/// <returns>The color with altered and brightness.</returns>
		public static Color ChangeColor(Color c,  double hRate, double sRate, double bRate)
		{
			if (double.IsNaN(hRate) || 
				double.IsNaN(sRate) || 
				double.IsNaN(bRate))
				return default(Color);

			if (M.Abs(hRate) < double.Epsilon && 
				M.Abs(sRate) < double.Epsilon && 
				M.Abs(bRate) < double.Epsilon)
				return c;

			var hsv = new Rgb{R = c.R, B = c.B, G = c.G}.To<Hsv>();

			hsv.H = GetNewValue(hsv.H, 360D, hRate);
			hsv.S = GetNewValue(hsv.S, 1D, sRate);
			hsv.V = GetNewValue(hsv.V, 1D, bRate);
			
			var rgb = hsv.ToRgb();
			return Color.FromArgb(c.A, (byte) rgb.R, (byte) rgb.G, (byte) rgb.B);
		}

		private static double GetNewValue(double oldValue, double max, double rate)
		{
			if (rate < -double.Epsilon)
				return  rate < -1
					? 0 
					: (1 + rate) * oldValue;

			if (rate > double.Epsilon)
				return rate > 1
					? 0
					: (max - oldValue) * rate + oldValue;

			return oldValue;
		}
	}
}