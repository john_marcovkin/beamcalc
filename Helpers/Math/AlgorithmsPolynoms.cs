﻿using System;
using MathNet.Numerics;
using MathNet.Numerics.RootFinding;

namespace Helpers.Math
{
	public partial class Algorithms
	{
		public static double GetMinPositiveCubicRoot(double a, double b, double c, double d, double upperBound)
		{
			var roots = Cubic.RealRoots(d / a, c / a, b / a);

			var m = new[] {double.PositiveInfinity, double.PositiveInfinity, double.PositiveInfinity};
			if (roots.Item1 >= 0) m[0] = roots.Item1; 
			if (roots.Item2 >= 0) m[1] = roots.Item2; 
			if (roots.Item3 >= 0) m[2] = roots.Item3;

			var min = Min(m);
			return min <= upperBound ? min : upperBound;
		}

		public static double GetMinPositiveQuadraticRoot(double a, double b, double c, double upperBound)
		{
			var roots = FindRoots.Quadratic(c, b, a);

			var r1 = System.Math.Abs(roots.Item1.Imaginary) < double.Epsilon && roots.Item1.Real >=0 
				? roots.Item1.Real 
				: double.PositiveInfinity;

			var r2 = System.Math.Abs(roots.Item2.Imaginary) < double.Epsilon && roots.Item2.Real >= 0
				? roots.Item2.Real
				: double.PositiveInfinity;

			var min = System.Math.Min(r1, r2);
			return min <= upperBound ? min  : upperBound;
		}
	}
}