﻿using Models;

namespace Helpers.Math
{
	public static partial class Algorithms
	{
		/// <summary>Н - кгс, G=round(9.8) = 10 m/sec2.</summary>
		public static double ConvertLoadValue(double value, MeasureUnitsSystem from, MeasureUnitsSystem to)
		{
			switch (from)
			{
				case MeasureUnitsSystem.Si when to == MeasureUnitsSystem.Tech: return value / 10D;
				case MeasureUnitsSystem.Tech when to == MeasureUnitsSystem.Si: return value * 10D;
				default: return value;
			}
		}

		/// <summary>м - см</summary>
		public static double ConvertLengthValue(double value, MeasureUnitsSystem from, MeasureUnitsSystem to)
		{
			switch (from)
			{
				case MeasureUnitsSystem.Si when to == MeasureUnitsSystem.Tech: return value * 100D;
				case MeasureUnitsSystem.Tech when to == MeasureUnitsSystem.Si: return value / 100D;
				default: return value;
			}
		}

		/// <summary>MПа - кГс/см2, G=round(9.8) = 10 m/sec2.</summary>
		public static double ConvertStressValue(double value, MeasureUnitsSystem from, MeasureUnitsSystem to)
		{
			switch (from)
			{
				case MeasureUnitsSystem.Si when to == MeasureUnitsSystem.Tech: return value * 10D;
				case MeasureUnitsSystem.Tech when to == MeasureUnitsSystem.Si: return value / 10D;
				default: return value;
			}
		}

		/// <summary>м2 - см2</summary>
		public static double ConvertAreaValue(double value, MeasureUnitsSystem from, MeasureUnitsSystem to)
		{
			switch (from)
			{
				case MeasureUnitsSystem.Si when to == MeasureUnitsSystem.Tech: return value * 10000D;
				case MeasureUnitsSystem.Tech when to == MeasureUnitsSystem.Si: return value / 10000D;
				default: return value;
			}
		}

		public static double ConvertMPaToPa(double value) => value * 1000000D;
		public static double ConvertPaToMPa(double value) => value * 0.000001D;
	}
}