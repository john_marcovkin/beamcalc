﻿using System;
using System.Linq;
using System.Windows;
using System.Collections.Generic;

namespace Helpers.Math
{
	public static partial class Algorithms
	{
		public static T Max<T>(IEnumerable<T> values) where T : IComparable<T>
		{
			var max = values.First();
			for (int i = 1; i < values.Count(); i++)
			{
				var value = values.ElementAt(i);
				if (value.CompareTo(max) > 0) max = value;
			}

			return max;
		}

		public static T Min<T>(IEnumerable<T> values) where T : IComparable<T>
		{
			var min = values.First();
			for (var i = 1; i < values.Count(); i++)
			{
				var value = values.ElementAt(i);
				if (value.CompareTo(min) < 0) min = value;
			}

			return min;
		}

		public static Size GetInscribeRectangle(Size outer, Size inner)
		{
			var otr = outer.Height / outer.Width;
			var inn = inner.Height / inner.Width;

			if (otr > inn)
				return new Size
				{
					Width = outer.Width,
					Height = outer.Width / inner.Width * outer.Height
				};
			else
				return new Size
				{
					Width = outer.Height / inner.Height * outer.Width,
					Height = outer.Height
				};
		}
	}
}