﻿using System.Windows;
using M = System.Math;


namespace Helpers.Math
{
	public static partial class Algorithms
	{
		private const double RadPerDegree = M.PI / 180;

		public static Thickness GetOverflowValue(Point canvasPosition, Size canvasSize, Size elementSize, double elementRotationAngle)
		{
			var cos = M.Cos(elementRotationAngle * RadPerDegree);
			var sin = M.Sin(elementRotationAngle * RadPerDegree);

			var prWx = elementSize.Width * cos;
			var prHx = -elementSize.Height * sin;

			var prWy = elementSize.Width * sin;
			var prHy = elementSize.Height * cos;

			var r = Max(new[] {canvasPosition.X, canvasPosition.X + prHx, canvasPosition.X + prWx, canvasPosition.X + prWx + prHx});
			var l = Min(new[] {canvasPosition.X, canvasPosition.X + prHx, canvasPosition.X + prWx, canvasPosition.X + prWx + prHx});

			var t = Min(new[] {canvasPosition.Y, canvasPosition.Y + prHy, canvasPosition.Y + prWy, canvasPosition.Y + prWy + prHy});
			var b = Max(new[] {canvasPosition.Y, canvasPosition.Y + prHy, canvasPosition.Y + prWy, canvasPosition.Y + prWy + prHy});

			var right = r - canvasSize.Width;
			var bottom = b - canvasSize.Height;

			return new Thickness
			{
				Top = t < 0 ? -t : 0,
				Bottom = bottom > 0 ? bottom : 0,

				Left = l < 0 ? -l : 0,
				Right = right > 0 ? right : 0
			};
		}
	}
}
